// For more info on raymarching, see:
//  http://9bitscience.blogspot.co.uk/2013/07/raymarching-distance-fields_14.html
//  http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
//  http://www.pouet.net/topic.php?which=7931&page=1


precision mediump float;

uniform float u_aspectRatio;
uniform mat4 u_view;

varying vec2 v_texCoord;



#define RM_STEP_MULTIPLIER  0.75
#define RM_TOLERANCE        0.05
#define RM_MAX_STEPS        20

#define NORMAL_EPSILON      0.01

#define AO_ITERATIONS       10
#define AO_STEP             0.3



float sdSphere(vec3 pos, float size) {
  return length(pos) - size;
}


float sdBox(vec3 pos, vec3 size) {
  vec3 d = abs(pos) - size;
  return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}


vec3 repeat(vec3 pos, vec3 range) {
    return mod(pos, range) - 0.5 * range;
}


float field(vec3 pos) {
    // The main field - a bunch of repeated cubes that intersect in interesting ways
    float s1 = sdBox(repeat(pos, vec3(4.0, 4.0, 4.0)), vec3(1.7, 1.7, 1.7));
    float s2 = sdBox(repeat(pos, vec3(3.0, 3.0, 3.0)), vec3(1.3, 1.3, 1.4));
    float s3 = sdBox(repeat(pos, vec3(1.0, 2.1, 1.2)), vec3(0.3, 0.2, 0.3));
    float s4 = sdBox(repeat(pos, vec3(6.0, 6.3, 6.0)), vec3(2.5, 2.2, 2.2));
    float s1234 = min(max(-s1, s2), max(s3, s4));

    // Carve a large sphere-shaped hole in the middle of everything
    float ss = sdSphere(pos - vec3(0, 0, 0), 30.0);
    return max(-ss, s1234);
}


vec3 getNormal(vec3 pos) {
   return normalize(vec3(field(pos + vec3(NORMAL_EPSILON, 0, 0)) - field(pos - vec3(NORMAL_EPSILON, 0, 0)), 
                         field(pos + vec3(0, NORMAL_EPSILON, 0)) - field(pos - vec3(0, NORMAL_EPSILON, 0)), 
                         field(pos + vec3(0, 0, NORMAL_EPSILON)) - field(pos - vec3(0, 0, NORMAL_EPSILON))));
}


float getAmbientOcclusion(vec3 pos, vec3 norm) {
    float o = 1.0;
	for (float i = float(AO_ITERATIONS); i > 0.0; i--)
	{
		o -= (i * AO_STEP - abs(field(pos + norm * i * AO_STEP))) / pow(2.0, i);
	}
	return o;
}


vec3 rayMarch(vec3 pos, vec3 rayDir) {
    vec3 currentPos = pos;
    float dist;
    for (int stepCounter = 0; stepCounter < RM_MAX_STEPS; stepCounter++) {
        dist = abs(field(currentPos));
        currentPos += dist * RM_STEP_MULTIPLIER * rayDir;
        if (dist < RM_TOLERANCE)
        {
            return vec3(float(stepCounter) / float(RM_MAX_STEPS), length(pos - currentPos), dist);
        }
    }
    return vec3(1, 0, 0);
}


void main() {
    // Get ray origin and direction
    vec3 origin = vec3(u_view[0][3], u_view[1][3], u_view[2][3]);
    vec3 target = vec3((v_texCoord.xy - 0.5) * 2.0 * vec2(u_aspectRatio, 1.0), 0);
    vec3 eye = vec3(0, 0, -2);  // This controls the field-of-view
    vec4 ray = u_view * vec4(normalize(target - eye), 0);

    // Ray march to find the position and normal that this ray hits
    vec3 res = rayMarch(origin, ray.xyz);
    vec3 pos = origin + ray.xyz * res.y;
    vec3 normal = getNormal(pos);

    // Start with some ambient light colour
    vec3 colour = vec3(0.1, 0.1, 0.1);

    // Add white-ish light shining down and forwards
    {
	vec3 lightNormal = normalize(vec3(0.0, 1.0, -0.8));
	float v = max(0.0, dot(normal, -lightNormal));
	colour += v * vec3(1.0, 0.9, 0.9);
	}

    // Add dark red-ish ligh shining straight up
    {
	vec3 lightNormal = normalize(vec3(0.0, -1.0, 0.0));
	float v = max(0.0, dot(normal, -lightNormal));
	colour += v * vec3(0.5, 0.2, 0.1);
	}

    // Add cyan glow based on how long the ray march took, aka: make the edges glow
    //colour += pow(res.x, 5.0) * vec3(0.2, 1.2, 1.6);

    // Multiply by ambient occlusion to darken the shadowed areas
	float ambientOcclusion = getAmbientOcclusion(pos, normal);
    colour *= ambientOcclusion;

    // Set the final colour to the fragment
    gl_FragColor = vec4(colour.rgb, 1.0);
}

