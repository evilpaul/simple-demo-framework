precision mediump float;

uniform sampler2D u_src;
uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


void main() {
    vec2 toMiddle = v_texCoord - 0.5;
    vec2 normToMiddle = normalize(toMiddle);
    float distFromMiddle = length(toMiddle * vec2(u_aspectRatio, 1.0));

    float maxOffset = 5.0;
    float offset = maxOffset * distFromMiddle;
    vec2 offsetR = u_pixelSize * normToMiddle * (distFromMiddle + offset);
    vec2 offsetG = u_pixelSize * normToMiddle * distFromMiddle;
    vec2 offsetB = u_pixelSize * normToMiddle * (distFromMiddle - offset);
    
    float r = texture2D(u_src, v_texCoord + offsetR).r;
    float g = texture2D(u_src, v_texCoord + offsetG).g;
    float b = texture2D(u_src, v_texCoord + offsetB).b;
    gl_FragColor = vec4(r, g, b, 1);
}
