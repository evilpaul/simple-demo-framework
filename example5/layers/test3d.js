function Layer_Test3D(args) {
    this.meshFilename = args["meshFilename"];
    this.cageFilename = args["cageFilename"];
    this.backgroundFragmentShader = args["backgroundFragmentShader"];
}


Layer_Test3D.prototype.getResourceList = function () {
    return [this.meshFilename, this.cageFilename, "passThrough.vert", this.backgroundFragmentShader];
}


Layer_Test3D.prototype.initialise = function () {
    this.mesh = resourceManager.getObject(this.meshFilename);
    this.cage = resourceManager.getObject(this.cageFilename);

    this.backgroundProgram = webGL.createProgram(resourceManager.getObject("passThrough.vert"), resourceManager.getObject(this.backgroundFragmentShader));

    this.camera = new Camera();
    this.camera.setTo(vec3Make(0, 0, 0));
    this.camPoints = [
        [15, 10, -40],
        [-3, -20, -20],
        [50, 5, -20],
        [-10, 15, -20],
    ];
}


Layer_Test3D.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(webGL.BLENDMODE_OPAQUE);

    // Calculate view
    var t = time / 4;
    var p0 = Math.floor(t) % this.camPoints.length;
    var p1 = (p0 + 1) % this.camPoints.length;
    var a = MixFunctions.easeInOutQuad(t % 1);
    var from = vec3Mix(this.camPoints[p0], this.camPoints[p1], a);
    this.camera.setFrom(from);
    var matViewProjection = this.camera.getViewProjection();

    // Render background
    var uniforms = {
        u_aspectRatio: webGL.getAspectRatio(),
        u_view: this.camera.matView,
    };
    if (this.uniforms) {
        for (var attrName in this.uniforms) { uniforms[attrName] = this.uniforms[attrName]; }
    }
    webGL.renderQuad(this.backgroundProgram, uniforms);

    // Render animated cubes
    var size = 3;
    for (var x = -size; x <= size; x++) {
        var xs = Math.sin(x * 0.46 + time * -1.35);
        for (var y = -size; y <= size; y++) {
            var ys = Math.sin(y * 0.71 + time * 1.12);
            for (var z = -size; z <= size; z++) {
                var zs = Math.sin(z * -0.32 + time * 1.73);
                var scale = Math.sin(xs + ys + zs) * 0.5 + 0.5;
                var matScale = mat4MakeScale(scale, scale, scale);
                var matTranslation = mat4MakeTranslation(x * 1, y * 1, z * 1);
                var matWVP = mat4MultiplyList([matViewProjection, matTranslation, matScale]);
                this.mesh.renderColoured(matWVP, [Math.abs(x / size), Math.abs(y / size), Math.abs(z / size), 1]);
            }
        }
    }

    // Render cage around the cubes
    var scale = 0.475;
    var matScale = mat4MakeScale(scale, scale, scale);
    var matWVP = mat4MultiplyList([matViewProjection, matScale]);
    this.cage.renderColoured(matWVP, [0.2, 0.2, 0.2, 1]);
}

