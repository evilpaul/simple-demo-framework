///////////////////////////////////////////////////////////////////////////////////////////////////
// Resource manager
// http://sonargame.com/2011/07/23/resource-loading-in-javascript/
///////////////////////////////////////////////////////////////////////////////////////////////////

//  Figure out what audio type this browser supports..
var _audioFileExtension = ".???";
(function() {
    console.log("Testing browser audio support..");
    _audioFileExtension = ".???";
    if (typeof(Audio) === "undefined") {
        alert("Your browser does not seem to support audio.. sorry :(");
        return;
    }
    var testAudio = new Audio();
    if (typeof(testAudio.canPlayType) !== "function") {
        alert("Your browser does not seem to support querying audio support.. sorry :(");
        return;
    }
    if (testAudio.canPlayType("audio/ogg") != "") {
        console.log("Using .ogg files");
        _audioFileExtension = ".ogg";
        return;
    } else if (testAudio.canPlayType("audio/mp3") != "") {
        console.log("Using .mp3 files");
        _audioFileExtension = ".mp3";
        return;
    }
    alert("Your browser does not seem to support .ogg or .mp3 files.. sorry :(");
} ());



function ResourceManager(baseDirectories) {
    if (typeof(baseDirectories) === "undefined") {
        this.baseDirectories = [""];
    } else if (typeof(baseDirectories) === "string") {
        this.baseDirectories = [baseDirectories];
    } else {
        this.baseDirectories = baseDirectories;
    }
    this.requested = {};
    this.resources = {};
    this.handlers = {};
    this.loadsPending = 0;

    LOGMSG("ResourceManager started with paths: " + this.baseDirectories);
}


ResourceManager.prototype.getObject = function (resourceName) {
    if (this.loadsPending > 0) {
        LOGMSG("resourceManager -> getObject('" + resourceName + "') with " + this.loadsPending + " pending");
    }

    if (resourceName in this.resources) {
        return this.resources[resourceName];
    } else {
        alert("Trying to get non-loaded resource '" + resourceName + "'");
        return null;
    }
}


// See if a specific resource is loaded
ResourceManager.prototype.isResourceLoaded = function (resourceName) {
    if (resourceName in this.resources) {
        return true;
    } else {
        return false;
    }
}


// See if there are any pending requests
ResourceManager.prototype.isAllLoaded = function() {
    return this.loadsPending == 0
}


// Request the loading of a file resource
// If you supply resourceName then the resource will be identified by that name, otherwise it will 
// be identified by the filename
ResourceManager.prototype.loadFile = function (filename, resourceName) {
    // If no resourceName is supplied then use the filename as the resourceName
    if (resourceName == undefined) {
        resourceName = filename;
    }

    // Are we already trying to load (or have already loaded) this resource?
    if (this.requested[resourceName] != undefined) {
        return;
    }
    this.requested[resourceName] = true;

    // Sneaky rename of audio files..
    if (endsWith(filename, ".ogg") || endsWith(filename, ".mp3")) {
        filename = filename.substr(0, filename.length - 4) + _audioFileExtension;

        // ..and sneaky shortcut load too
        for (var handler in this.handlers) {
            if (endsWith(filename, handler)) {
                this.resources[resourceName] = this.handlers[handler](null, "resources/" + filename);
                return;
            }
        }
    }

    // Start the request
    LOGMSG("ResourceManager kicking off request for " + filename);
    if (window.XMLHttpRequest) {
        this._request()(resourceName, filename, 0);
    }
}

ResourceManager.prototype._request = function () {
    var self = this;
    return function (resourceName, filename, directoryIndex) {
        var request = new XMLHttpRequest();
        url = self.baseDirectories[directoryIndex] + filename;
        request.open('GET', url);
        LOGMSG("Trying to find '" + filename + "' at '" + url + "'");
        request.responseType = 'arraybuffer';
        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                if (request.status == 200 || request.state == 304 /*|| request.status == 206*/ || request.status == 0) {
                    LOGMSG("ResourceManager completed request for " + filename + " (" + request.status + ")");
                    var handled = false;
                    var byteArray = new Uint8Array(request.response);
                    for (var handler in self.handlers) {
                        if (endsWith(filename, handler)) {
                            self.resources[resourceName] = self.handlers[handler](byteArray, filename);
                            handled = true;
                            break;
                        }
                    }
                    if (!handled) {
                        self.resources[resourceName] = byteArray;
                    }
                } else if (request.status == 404) {
                    directoryIndex++;
                    if (directoryIndex < self.baseDirectories.length) {
                        self._request()(resourceName, filename, directoryIndex);
                    } else {
                        alert("Failed to find file '" + filename + "' in any resource directory");
                    }
                } else {
                    alert("Failed to load file '" + filename + "'! HTTP status: " + request.status);
                }
                self.loadsPending--;
                if (self.loadsPending == 0) {
                    LOGMSG("ResourceManager -> all loads completed");
                }
            }
        }
        request.send(null);
        self.loadsPending++;
    };
}


// Register a handler for a specific resource type
// Pass in extensions as a single string to handle a single extension, or as a list of strings to 
// handle multiple extensions
ResourceManager.prototype.registerHandler = function (handler, extensions) {
    if (typeof(extensions) == "string") {
        this.handlers[extensions] = handler;
    } else {
        for (var i in extensions) {
            var extension = extensions[i];
            this.handlers[extension] = handler;
        }
    }
}