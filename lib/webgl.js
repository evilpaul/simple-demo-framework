function WebGL(canvas) {
    // Get the rendering context
    this.glContext = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    if (!this.glContext) {
        alert("Couldn't get WebGL context on canvas");
        return;
    }

    // Load required extensions
    var requiredExtenstions = ["WEBGL_depth_texture"];
    for (var i in requiredExtenstions)
    {
        var extensionName = requiredExtenstions[i];
        if (!this.glContext.getExtension(extensionName))
        {
            alert("Couldn't load WebGL extension " + extensionName);
            return;
        }
    }

    // Remember size and aspect
    this.resolution = vec2Make(canvas.width, canvas.height);
    this.aspectRatio = canvas.width / canvas.height;
    this.aspectScalingMat = mat3MakeScale(1, this.aspectRatio);
    this.pixelSize = vec2Make(1 / canvas.width, 1 / canvas.height);

    // Constants
    this.BLENDMODE_OPAQUE = 1000;
    this.BLENDMODE_ALPHA = 1001;
    this.BLENDMODE_ADDITIVE = 1002;
    this.BLENDMODE_SUBTRACTIVE = 1003;
    this.TEXTURE_WRAPMODE_WRAP = 2000;
    this.TEXTURE_WRAPMODE_CLAMP = 2001;
    this.TEXTURE_SAMPLEMODE_LINEAR = 3000;
    this.TEXTURE_SAMPLEMODE_NEAREST = 3001;

    // Standard buffers
    this.standardScreenQuadBuffer = this.createBuffer(this.glContext.ARRAY_BUFFER, new Float32Array([
        -1.0, -1.0,
         1.0, -1.0,
        -1.0, 1.0,
        -1.0, 1.0,
         1.0, -1.0,
         1.0, 1.0]));
    this.standardScreenUVBuffer = this.createBuffer(this.glContext.ARRAY_BUFFER, new Float32Array([
        0.0, 1.0,
        1.0, 1.0,
        0.0, 0.0,
        0.0, 0.0,
        1.0, 1.0,
        1.0, 0.0]));

    // Initial configuration
    this.glContext.pixelStorei(this.glContext.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
    this.glContext.enable(this.glContext.BLEND); this.lastBlendMode = 0; this.setBlendMode(this.BLENDMODE_ALPHA);
    this.glContext.depthFunc(this.glContext.LEQUAL);

    // Debug texture data
    this.debugTextures = [];
    resourceManager.loadFile("transform.vert");
    resourceManager.loadFile("image.frag");
    resourceManager.loadFile("1x1black.png");
    this.debugTextureProgram = null;
}


WebGL.prototype.getResolution = function () {
    return this.resolution;
}


WebGL.prototype.getAspectRatio = function () {
    return this.aspectRatio;
}


WebGL.prototype.getAspectScalingMat = function () {
    return this.aspectScalingMat;
}


WebGL.prototype.getPixelSize = function () {
    return this.pixelSize;
}


WebGL.prototype.createVertexShader = function (vertexShaderSource){
    var vertexShader = this.createShader(this.glContext.VERTEX_SHADER, vertexShaderSource);
    return vertexShader;
}


WebGL.prototype.createFragmentShader = function (fragmentShaderSource){
    var fragmentShaderSource = this.createShader(this.glContext.FRAGMENT_SHADER, fragmentShaderSource);
    return fragmentShaderSource;
}


WebGL.prototype.createProgram = function (vertexShader, fragmentShader){
    var program = this.glContext.createProgram();
    this.glContext.attachShader(program, vertexShader);
    this.glContext.attachShader(program, fragmentShader);
    this.glContext.linkProgram(program);
    var linked = this.glContext.getProgramParameter(program, this.glContext.LINK_STATUS);
    if (!linked) {
        var lastError = this.glContext.getProgramInfoLog(program);
        LOGMSG("Error in program linking: " + lastError);
        this.glContext.deleteProgram(program);
        return null;
    } else {
        return program;
    }
}


WebGL.prototype.createTexture = function (image) {
    var texture = this.glContext.createTexture();
    this.glContext.bindTexture(this.glContext.TEXTURE_2D, texture);
    this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_S, this.glContext.CLAMP_TO_EDGE);
    this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_T, this.glContext.CLAMP_TO_EDGE);
    this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_MIN_FILTER, this.glContext.LINEAR);
    this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_MAG_FILTER, this.glContext.LINEAR);
    if (image != null) {
        this.glContext.texImage2D(this.glContext.TEXTURE_2D, 0, this.glContext.RGBA, this.glContext.RGBA, this.glContext.UNSIGNED_BYTE, image);
    }
    return texture;
}


WebGL.prototype.createFramebuffer = function () {
    // Create fbo
    var frameBufferObject = this.glContext.createFramebuffer();

    // Create diffuse texture
    var diffuseTexture = this.createTexture(null);
    this.glContext.bindTexture(this.glContext.TEXTURE_2D, diffuseTexture);
    this.glContext.texImage2D(this.glContext.TEXTURE_2D, 0, this.glContext.RGBA, this.resolution[0], this.resolution[1], 0, this.glContext.RGBA, this.glContext.UNSIGNED_BYTE, null);

    // Create depth texture
    var depthTexture = this.glContext.createTexture();
    this.glContext.bindTexture(this.glContext.TEXTURE_2D, depthTexture);
    this.glContext.texImage2D(
        this.glContext.TEXTURE_2D,
        0,
        this.glContext.DEPTH_COMPONENT,
        this.resolution[0],
        this.resolution[1],
        0,
        this.glContext.DEPTH_COMPONENT,
        this.glContext.UNSIGNED_INT,
        null);

    // Store everthing on a new object and return it
    var newFrameBuffer = { "frameBufferObject": frameBufferObject, "diffuseTexture": diffuseTexture, "depthTexture": depthTexture };
    return newFrameBuffer;
}


WebGL.prototype.bindFramebuffer = function (frameBuffer) {
    if (frameBuffer) {
        this.glContext.bindFramebuffer(this.glContext.FRAMEBUFFER, frameBuffer["frameBufferObject"]);
        this.glContext.framebufferTexture2D(this.glContext.FRAMEBUFFER, this.glContext.COLOR_ATTACHMENT0, this.glContext.TEXTURE_2D, frameBuffer["diffuseTexture"], 0);
        this.glContext.framebufferTexture2D(this.glContext.FRAMEBUFFER, this.glContext.DEPTH_ATTACHMENT, this.glContext.TEXTURE_2D, frameBuffer["depthTexture"], 0);
    }
    else {
        this.glContext.bindFramebuffer(this.glContext.FRAMEBUFFER, null);
    }
}


WebGL.prototype.clearScreen = function (colour, depth) {
    if (typeof colour === "undefined") this.glContext.clearColor(0, 0, 0, 0); else this.glContext.clearColor(colour[0], colour[1], colour[2], colour[3])
    if (typeof depth === "undefined") this.glContext.clearDepth(1.0); else this.glContext.clearDepth(depth);
    this.glContext.clear(this.glContext.COLOR_BUFFER_BIT | this.glContext.DEPTH_BUFFER_BIT);
}


WebGL.prototype.createBuffer = function (type, arrayData) {
    var buffer = this.glContext.createBuffer();
    this.glContext.bindBuffer(type, buffer);
    this.glContext.bufferData(type, arrayData, this.glContext.STATIC_DRAW);
    return buffer;
}


WebGL.prototype.renderQuad = function (program, uniforms) {
    // Don't want the depth buffer or culling
    this.glContext.disable(this.glContext.DEPTH_TEST);
    this.glContext.disable(this.glContext.CULL_FACE);

    // Set the shader program
    this.glContext.useProgram(program);

    // Set position data
    var positionLocation = this.glContext.getAttribLocation(program, "a_position");
    this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, this.standardScreenQuadBuffer);
    this.glContext.enableVertexAttribArray(positionLocation);
    this.glContext.vertexAttribPointer(positionLocation, 2, this.glContext.FLOAT, false, 0, 0);
    
    // Set UV data
    var texCoordLocation = this.glContext.getAttribLocation(program, "a_texCoord");
    this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, this.standardScreenUVBuffer);
    this.glContext.enableVertexAttribArray(texCoordLocation);
    this.glContext.vertexAttribPointer(texCoordLocation, 2, this.glContext.FLOAT, false, 0, 0);

    // Set uniforms
    this.setUniforms(program, uniforms);

    // Draw
    this.glContext.drawArrays(this.glContext.TRIANGLES, 0, 6);
}


WebGL.prototype.renderIndexedTriangles = function (program, uniforms, vertBuffer, attributes, indexBuffer, numIndices) {
    // Do want the depth buffer and culling
    this.glContext.enable(this.glContext.DEPTH_TEST);
    this.glContext.enable(this.glContext.CULL_FACE);

    // Set the shader program
    this.glContext.useProgram(program);

    // Uniforms
    this.setUniforms(program, uniforms);

    // Set position data
    var positionLocation = this.glContext.getAttribLocation(program, "a_position");
    this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, vertBuffer);
    this.glContext.enableVertexAttribArray(positionLocation);
    this.glContext.vertexAttribPointer(positionLocation, 3, this.glContext.FLOAT, false, 0, 0);

    // Attributes
    for (key in attributes) {
        var location = this.glContext.getAttribLocation(program, key);
        if (location != null) {
            var value = attributes[key];
            var type = Object.prototype.toString.call(value);
            if (value == null) {
                this.glContext.disableVertexAttribArray(location);
            } else if (type == "[object WebGLBuffer]") {
                this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, value);
                this.glContext.enableVertexAttribArray(location);
                this.glContext.vertexAttribPointer(location, 2, this.glContext.FLOAT, false, 0, 0);
            }
        }
    }

    // Indices
    this.glContext.bindBuffer(this.glContext.ELEMENT_ARRAY_BUFFER, indexBuffer);

    // Draw
    this.glContext.drawElements(this.glContext.TRIANGLES, numIndices, this.glContext.UNSIGNED_SHORT, 0);
}


WebGL.prototype.setUniforms = function (program, uniforms) {
    var textureId = 0;
    for (var key in uniforms) {
        var location = this.glContext.getUniformLocation(program, key);
        if (location != null) {
            var value = uniforms[key];
            var type = Object.prototype.toString.call(value);
            if (type == "[object Number]") {
                this.glContext.uniform1f(location, value);
            } else if (type == "[object WebGLTexture]") {
                this.glContext.uniform1i(location, textureId);
                this.glContext.activeTexture(this.glContext.TEXTURE0 + textureId);
                this.glContext.bindTexture(this.glContext.TEXTURE_2D, value);
                textureId++;
            } else if (type == "[object Float32Array]" || type == "[object Array]") {
                var length = value.length;
                if (length == 2) {
                    this.glContext.uniform2f(location, value[0], value[1]);
                } else if (length == 3) {
                    this.glContext.uniform3f(location, value[0], value[1], value[2]);
                } else if (length == 4) {
                    this.glContext.uniform4f(location, value[0], value[1], value[2], value[3]);
                } else if (length == 9) {
                    this.glContext.uniformMatrix3fv(location, false, mat3Transpose(value));
                } else if (length == 16) {
                    this.glContext.uniformMatrix4fv(location, false, value);
                }
            }
        }
    }
}


WebGL.prototype.createShader = function(shaderType, shaderSource) {
    var shader = this.glContext.createShader(shaderType);
    this.glContext.shaderSource(shader, shaderSource);
    this.glContext.compileShader(shader);
    var compiled = this.glContext.getShaderParameter(shader, this.glContext.COMPILE_STATUS);
    if (!compiled) {
        var lastError = this.glContext.getShaderInfoLog(shader);
        var type = shaderType == this.glContext.VERTEX_SHADER ? "vertex" : "fragment";
        LOGMSG("Error compiling " + type + " shader: " + lastError);
        this.glContext.deleteShader(shader);
        return null;
    } else {
        return shader;
    }
}


WebGL.prototype.setBlendMode = function (blendMode) {
    if (blendMode != this.lastBlendMode) {
        switch (blendMode) {
            case this.BLENDMODE_OPAQUE:
                this.glContext.blendFunc(this.glContext.ONE, this.glContext.ZERO);
                break;
            case this.BLENDMODE_ALPHA:
                this.glContext.blendFunc(this.glContext.SRC_ALPHA, this.glContext.ONE_MINUS_SRC_ALPHA);
                break;
            case this.BLENDMODE_ADDITIVE:
                this.glContext.blendFunc(this.glContext.ONE, this.glContext.ONE);
                break;
            case this.BLENDMODE_MULTIPLICATIVE:
                this.glContext.blendFunc(this.glContext.DST_COLOR, this.glContext.ZERO);
                break;
            default:
                LOGMSG("Unknown blend mode");
        }
        this.lastBlendMode = blendMode;
    }
}


WebGL.prototype.getBlendModeFromString = function (blendMode) {
    if (blendMode == "opaque") return this.BLENDMODE_OPAQUE;
    if (blendMode == "alpha") return this.BLENDMODE_ALPHA;
    if (blendMode == "additive") return this.BLENDMODE_ADDITIVE;
    if (blendMode == "multiplicative") return this.BLENDMODE_MULTIPLICATIVE;
}


WebGL.prototype.setTextureWrapMode = function (texture, wrapMode) {
    this.glContext.bindTexture(this.glContext.TEXTURE_2D, texture);
    switch (wrapMode) {
        case this.TEXTURE_WRAPMODE_WRAP:
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_S, this.glContext.REPEAT);
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_T, this.glContext.REPEAT);
            break;
        case this.TEXTURE_WRAPMODE_CLAMP:
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_S, this.glContext.CLAMP_TO_EDGE);
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_T, this.glContext.CLAMP_TO_EDGE);
            break;
        default:
            LOGMSG("Unknown texture wrap mode");
    }
}


WebGL.prototype.getTextureWrapModeFromString = function (wrapMode) {
    if (wrapMode == "wrap") return this.TEXTURE_WRAPMODE_WRAP;
    if (wrapMode == "clamp") return this.TEXTURE_WRAPMODE_CLAMP;
}


WebGL.prototype.setTextureSampleMode = function (texture, sampleMode) {
    this.glContext.bindTexture(this.glContext.TEXTURE_2D, texture);
    switch (sampleMode) {
        case this.TEXTURE_SAMPLEMODE_LINEAR:
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_MIN_FILTER, this.glContext.LINEAR);
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_MAG_FILTER, this.glContext.LINEAR);
            break;
        case this.TEXTURE_SAMPLEMODE_NEAREST:
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_MIN_FILTER, this.glContext.NEAREST);
            this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_MAG_FILTER, this.glContext.NEAREST);
            break;
        default:
            LOGMSG("Unknown texture sample mode");
    }
}


WebGL.prototype.getTextureSampleModeFromString = function (sampleMode) {
    if (sampleMode == "linear") return this.TEXTURE_SAMPLEMODE_LINEAR;
    if (sampleMode == "nearest") return this.TEXTURE_SAMPLEMODE_NEAREST;
}


WebGL.prototype.addDebugTexture = function (texture) {
    this.debugTextures.push(texture);
}


WebGL.prototype.renderAndFlushDebugTexture = function () {
    if (this.debugTextures.length) {
        // Load the shader program as soon as possible
        if (this.debugTextureProgram == null &&
            resourceManager.isResourceLoaded("transform.vert") &&
            resourceManager.isResourceLoaded("image.frag")) {
            var vertexShader = this.createVertexShader(resourceManager.getObject("transform.vert"));
            var fragmentShader = this.createFragmentShader(resourceManager.getObject("image.frag"));
            this.debugTextureProgram = this.createProgram(vertexShader, fragmentShader);
            this.debugTextureBackground = webGL.createTexture(resourceManager.getObject("1x1black.png"));
        }
         
        // Show the debug textures
        if (this.debugTextureProgram) {
            var matAspect = this.getAspectScalingMat();
            var matScaleOuter = mat3MakeScale(0.1, -0.1);
            var matScaleInner = mat3MakeScale(0.09, -0.09);

            this.setBlendMode(this.BLENDMODE_ALPHA);

            var x = -0.85;
            var y = 0.5;
            for (var i in this.debugTextures) {
                var matTranslate = mat3MakeTranslation(x, y);
                this.renderQuad(this.debugTextureProgram, { u_transform: mat3Multiply(matTranslate, matScaleOuter), u_image: this.debugTextureBackground });
                this.renderQuad(this.debugTextureProgram, { u_transform: mat3Multiply(matTranslate, matScaleInner), u_image: this.debugTextures[i] });
                x += 0.225;
            }
        }

        // Flush
        this.debugTextures = [];
    }
}


