function Camera() {
    this.from = vec3Make(0, 0, -100);
    this.to = vec3Make(0, 0, 0);
    this.up = vec3Make(0, 1, 0);
    this.viewDirty = true;

    this.fov = Math.PI;
    this.zmin = 0.1;
    this.zmax = 500;
    this.projectionDirty = true;
}


Camera.prototype.setFrom = function (from) {
    this.from = from;
    this.viewDirty = true;
}


Camera.prototype.setTo = function (to) {
    this.to = to;
    this.viewDirty = true;
}


Camera.prototype.setUp = function (up) {
    this.up = up;
    this.viewDirty = true;
}


Camera.prototype.setFOV = function (fov) {
    this.fov = fov;
    this.projectionDirty = true;
}


Camera.prototype.setRange = function (zmin, zmax) {
    this.zmin = zmin;
    this.zmax = zmax;
    this.projectionDirty = true;
}


Camera.prototype.getViewProjection = function () {
    if (this.viewDirty == true) {
        this.matView = mat4MakeLookAt(this.from, this.to, this.up);
        this.viewDirty = false;
        this.viewProjectionDirty = true;
    }

    if (this.projectionDirty == true) {
        this.matProjection = mat4MakePerspective(this.fov, webGL.getAspectRatio(), this.zmin, this.zmax);
        this.projectionDirty = false;
        this.viewProjectionDirty = true;
    }

    if (this.viewProjectionDirty == true) {
        this.matViewProjection = mat4Multiply(this.matProjection, this.matView);
        this.viewProjectionDirty = false;
    }

    return this.matViewProjection;
}

