var logLines = []
function LOGMSG(msg) {
    var fullMsg = "[LOG] " + msg;

    if (document.getElementById("debugText")) {
        element = document.getElementById("debugText")
        logLines.push(fullMsg);
        if (logLines.length > 20) {
            logLines.shift();
        }
        element.innerHTML = "";
        for (line in logLines) {
            element.innerHTML += logLines[line] + "\n";
        }
    }

    console.log(fullMsg);
}


function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}


// query string: ?foo=lorem&bar=&baz
//  var foo = getParameterByName('foo'); // "lorem"
//  var bar = getParameterByName('bar'); // "" (present with empty value)
//  var baz = getParameterByName('baz'); // "" (present with no value)
//  var qux = getParameterByName('qux'); // null (absent)
// From: http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik M�ller
// fixes from Paul Irish and Tino Zijdel
(function () {

    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];

    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {

        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];

    }

    if (window.requestAnimationFrame === undefined) {

        window.requestAnimationFrame = function (callback, element) {

            var currTime = Date.now(), timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () { callback(currTime + timeToCall); }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;

        };

    }

    window.cancelAnimationFrame = window.cancelAnimationFrame || function (id) { window.clearTimeout(id) };
}());
