function toRadian(deg) {
    return 0.01745329251994329576923690768489 * deg;
}


// Easing functions
// Borrows heavily from https://gist.github.com/gre/1650294
function mix(v1, v2, a) {
    return v1 + (v2 - v1) * a;
}
MixFunctions = {
    linear: function (t) { return t },
    easeInQuad: function (t) { return t * t },
    easeOutQuad: function (t) { return t * (2 - t) },
    easeInOutQuad: function (t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t },
};


// ---------- vec2 ----------
function vec2Make(x, y) {
    return [x, y];
}

function vec2MakeRandomNormal() {
    var r, l;
    do {
        r = vec2Make(Math.random() * 2 - 1, Math.random() * 2 - 1);
        l = vec2Length(r);
    } while (l > 1);
    var il = 1 / l;
    return [r[0] * il, r[1] * il];
}

function vec2Add(a, b) {
    return [a[0] + b[0], a[1] + b[1]];
}

function vec2Subtract(a, b) {
    return [a[0] - b[0], a[1] - b[1]];
}

function vec2Scale(a, b) {
    return [a[0] * b, a[1] * b];
}

function vec2Multiply(a, b) {
    return [a[0] * b[0], a[1] * b[1]];
}

function vec2Length(a) {
    var x = a[0], y = a[1];
    return Math.sqrt(x * x + y * y);
}

function vec2Normalise(a) {
    var x = a[0], y = a[1], l = x * x + y * y;
    if (l > 0) {
        var il = 1 / Math.sqrt(l);
        return [x * il, y * il];
    } else {
        return [0, 0];
    }
}

function vec2Dot(a, b) {
    return a[0] * b[0] + a[1] * b[1];
}

function vec2Mix(v1, v2, a) {
    return [v1[0] + (v2[0] - v1[0]) * a,
            v1[1] + (v2[1] - v1[1]) * a];
}

function vec2Equals(a, b) {
    return a[0] == b[0] &&
           a[1] == b[1];
}


// ---------- vec3 ----------
function vec3Make(x, y, z) {
    return [x, y, z];
}

function vec3MakeRandomNormal() {
    var r, l;
    do {
        r = vec3Make(Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1);
        l = vec3Length(r);
    } while (l > 1);
    var il = 1 / l;
    return [r[0] * il, r[1] * il, r[2] * il];
}

function vec3Add(a, b) {
    return [a[0] + b[0], a[1] + b[1], a[2] + b[2]];
}

function vec3Subtract(a, b) {
    return [a[0] - b[0], a[1] - b[1], a[2] - b[2]];
}

function vec3Subtract(a, b) {
    return [a[0] - b[0], a[1] - b[1], a[2] - b[2]];
}

function vec3Scale(a, b) {
    return [a[0] * b, a[1] * b, a[2] * b];
}

function vec3Multiply(a, b) {
    return [a[0] * b[0], a[1] * b[1], a[2] * b[2]];
}

function vec3Length(a) {
    var x = a[0], y = a[1], z = a[2];
    return Math.sqrt(x * x + y * y + z * z);
}

function vec3Normalise(a) {
    var x = a[0], y = a[1], z = a[2], l = x * x + y * y + z * z;
    if (l > 0) {
        var il = 1 / Math.sqrt(l);
        return [x * il, y * il, z * il];
    } else {
        return [0, 0, 0];
    }
}

function vec3Dot(a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

vec3Cross = function (a, b) {
    var ax = a[0], ay = a[1], az = a[2],
        bx = b[0], by = b[1], bz = b[2];
    return [ay * bz - az * by, az * bx - ax * bz, ax * by - ay * bx];
};

function vec3Mix(v1, v2, a) {
    return [v1[0] + (v2[0] - v1[0]) * a,
            v1[1] + (v2[1] - v1[1]) * a,
            v1[2] + (v2[2] - v1[2]) * a];
}

function vec3Equals(a, b) {
    return a[0] == b[0] &&
           a[1] == b[1] &&
           a[2] == b[2];
}


// ---------- mat3 ----------
function mat3MakeTranslation(tx, ty) {
    return [1, 0, 0,
            0, 1, 0,
            tx, ty, 1];
}


function mat3MakeScale(sx, sy) {
    return [sx, 0, 0,
            0, sy, 0,
            0, 0, 1];
}


function mat3MakeRotation(rad) {
    var s = Math.sin(rad), c = Math.cos(rad);
    return [c, -s, 0,
            s, c, 0,
            0, 0, 1];
}


function mat3Multiply(a, b) {
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8],
        b00 = b[0], b01 = b[1], b02 = b[2],
        b10 = b[3], b11 = b[4], b12 = b[5],
        b20 = b[6], b21 = b[7], b22 = b[8];
    return [b00 * a00 + b01 * a10 + b02 * a20,
            b00 * a01 + b01 * a11 + b02 * a21,
            b00 * a02 + b01 * a12 + b02 * a22,
            b10 * a00 + b11 * a10 + b12 * a20,
            b10 * a01 + b11 * a11 + b12 * a21,
            b10 * a02 + b11 * a12 + b12 * a22,
            b20 * a00 + b21 * a10 + b22 * a20,
            b20 * a01 + b21 * a11 + b22 * a21,
            b20 * a02 + b21 * a12 + b22 * a22];
}


function mat3MultiplyList(a) {
    var r = a[0];
    for (var i = 1; i < a.length; i++) {
        r = mat3Multiply(r, a[i]);
    }
    return r;
}


function mat3Transpose(a) {
    return [a[0], a[3], a[6],
            a[1], a[4], a[7],
            a[2], a[5], a[8]];
}


// ---------- mat4 ----------
function mat4MakeTranslation(tx, ty, tz) {
    return [1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            tx, ty, tz, 1];
}


function mat4MakeScale(sx, sy, sz) {
    return [sx, 0, 0, 0,
            0, sy, 0, 0,
            0, 0, sz, 0,
            0, 0, 0, 1];
}


function mat4MakeRotationX(rad) {
    var s = Math.sin(rad), c = Math.cos(rad);
    return [1, 0, 0, 0,
            0, c, -s, 0,
            0, s, c, 0,
            0, 0, 0, 1];
}


function mat4MakeRotationY(rad) {
    var s = Math.sin(rad), c = Math.cos(rad);
    return [c, 0, s, 0,
            0, 1, 0, 0,
            -s, 0, c, 0,
            0, 0, 0, 1];
}


function mat4MakeRotationZ(rad) {
    var s = Math.sin(rad), c = Math.cos(rad);
    return [c, -s, 0, 0,
            s, c, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1];
}


function mat4MakePerspective(fov, aspect, near, far) {
    var rangeInv = 1.0 / (near - far);
    return [(fov / aspect), 0, 0, 0,
            0, fov, 0, 0,
            0, 0, (near + far) * rangeInv, -1,
            0, 0, near * far * rangeInv * 2, 0];
}


function mat4MakeLookAt(from, to, up) {
    var x0, x1, x2, y0, y1, y2, z0, z1, z2, len,
        fromx = from[0],
        fromy = from[1],
        fromz = from[2],
        upx = up[0],
        upy = up[1],
        upz = up[2],
        tox = to[0],
        toy = to[1],
        toz = to[2];

    z0 = fromx - tox;
    z1 = fromy - toy;
    z2 = fromz - toz;

    len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;

    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
    if (!len) {
        x0 = 0;
        x1 = 0;
        x2 = 0;
    } else {
        len = 1 / len;
        x0 *= len;
        x1 *= len;
        x2 *= len;
    }

    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;

    len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
    if (!len) {
        y0 = 0;
        y1 = 0;
        y2 = 0;
    } else {
        len = 1 / len;
        y0 *= len;
        y1 *= len;
        y2 *= len;
    }

    return [x0, y0, z0, 0,
            x1, y1, z1, 0,
            x2, y2, z2, 0,
            -(x0 * fromx + x1 * fromy + x2 * fromz), -(y0 * fromx + y1 * fromy + y2 * fromz), -(z0 * fromx + z1 * fromy + z2 * fromz), 1];
}

function mat4Multiply(a, b) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],
        b00 = b[0], b01 = b[1], b02 = b[2], b03 = b[3],
        b10 = b[4], b11 = b[5], b12 = b[6], b13 = b[7],
        b20 = b[8], b21 = b[9], b22 = b[10], b23 = b[11],
        b30 = b[12], b31 = b[13], b32 = b[14], b33 = b[15];
    return [b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30,
            b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31,
            b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32,
            b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33,
            b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30,
            b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31,
            b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32,
            b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33,
            b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30,
            b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31,
            b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32,
            b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33,
            b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30,
            b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31,
            b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32,
            b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33];
}


function mat4MultiplyList(a) {
    var r = a[0];
    for (var i = 1; i < a.length; i++) {
        r = mat4Multiply(r, a[i]);
    }
    return r;
}
