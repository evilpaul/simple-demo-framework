attribute vec2 a_position;
attribute vec2 a_texCoord;

uniform mat3 u_transform;

varying vec2 v_texCoord;


void main() {
    vec3 pos = vec3(a_position.x, a_position.y, 1.0);
    vec3 transformedPos = pos * u_transform;
    gl_Position = vec4(transformedPos.x, transformedPos.y, 0.0, 1.0);
    v_texCoord = a_texCoord;
}
