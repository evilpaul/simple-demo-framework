function Layer_FullScreenEffect(args) {
    this.imageFilename = args["imageFilename"];
    this.imageWrapMode = webGL.getTextureWrapModeFromString((typeof args["imageWrapMode"] !== "undefined") ? args["imageWrapMode"] : "wrap");
    this.fragmentShader = args["fragmentShader"];
    this.uniforms = (typeof args["uniforms"] !== "undefined") ? args["uniforms"] : {};
    this.isFilterEffect = true;
}


Layer_FullScreenEffect.prototype.getResourceList = function () {
    var resourceList = ["effect.vert", this.fragmentShader];
    if (this.imageFilename) resourceList.push(this.imageFilename);
    return resourceList;
}


Layer_FullScreenEffect.prototype.initialise = function () {
    this.program = webGL.createProgram(resourceManager.getObject("effect.vert"), resourceManager.getObject(this.fragmentShader));
    if (this.imageFilename) {
        this.texture = webGL.createTexture(resourceManager.getObject(this.imageFilename));
        webGL.setTextureWrapMode(this.texture, this.imageWrapMode);
    }
}


Layer_FullScreenEffect.prototype.render = function (time, timeDelta, sourceTexture, targetFbo) {
    var uniforms = {
        u_time: time,
        u_pixelSize: webGL.getPixelSize(),
        u_aspectRatio: webGL.getAspectRatio(),
        u_src: sourceTexture,
        u_image: this.texture,
    };
    if (this.uniforms) {
        for (var attrName in this.uniforms) { uniforms[attrName] = this.uniforms[attrName]; }
    }

    webGL.bindFramebuffer(targetFbo);

    webGL.setBlendMode(webGL.BLENDMODE_OPAQUE);
    webGL.renderQuad(this.program, uniforms);
}

