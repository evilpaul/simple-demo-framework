function Layer_Group(args) {
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "opaque");
    this.clearColour = (typeof args["clearColour"] !== "undefined") ? args["clearColour"] : [0, 0, 0, 0];

    // Create all layers of this group
    this.layerDefs = [];
    for (var i in args["layers"]) {
        var layerData = args["layers"][i];
        var type = layerData["type"];
        var name = (typeof layerData.name !== "undefined") ? layerData.name : "Unnamed layer";
        var start = (typeof layerData.start !== "undefined") ? layerData.start : 0;
        var length = (typeof layerData.length !== "undefined") ? layerData.length : 9999;
        var loop = (typeof layerData.loop !== "undefined") ? layerData.loop : false;

        // Create layer..
        try {
            var newLayer = new window["Layer_" + type](layerData);
        }
        catch (e) {
            LOGMSG("Failed to create layer type '" + type + "'. Did you forget to include the file in index.html?");
            throw(e);
        }

        // .. load resources..
        if (typeof (newLayer.getResourceList) == "function") {
            var layerResourceList = newLayer.getResourceList();
            for (var i in layerResourceList) {
                resourceManager.loadFile(layerResourceList[i]);
            }
        }

        // ..and add it to our list
        var newLayerDef = {};
        newLayerDef.layer = newLayer;
        newLayerDef.name = name;
        newLayerDef.type = type;
        newLayerDef.start = start;
        newLayerDef.length = length;
        newLayerDef.loop = loop;
        this.layerDefs.push(newLayerDef);

    }

    // Shader for drawing the final framebuffer to the canvas
    resourceManager.loadFile("effect.vert");
    resourceManager.loadFile("image.frag");

    this.isFilterEffect = true;
}


Layer_Group.prototype.getResourceList = function () {
}


Layer_Group.prototype.initialise = function () {
    LOGMSG("Group initialising " + this.layerDefs.length + " layers");
    for (var i in this.layerDefs) {
        var layerDef = this.layerDefs[i];
        var layer = layerDef.layer;
        if (typeof (layer.initialise) == "function") {
            try {
                layer.initialise();
            }
            catch (e) {
                LOGMSG("Exception thrown during initialisation of '" + layerDef.name + "', type '" + layerDef.type + "': " + e.toString());
            }
        }
    }

    // Create a couple of framebuffers for effects
    this.frameBuffers = [];
    for (var i = 0; i < 2; i++) {
        this.frameBuffers.push(webGL.createFramebuffer());
    }

    // And a shader to do the final render to canvas
    this.program = webGL.createProgram(resourceManager.getObject("effect.vert"), resourceManager.getObject("image.frag"));
}


Layer_Group.prototype.render = function (time, timeDelta, sourceTexture, targetFbo) {
    // Render to our framebuffer
    var fboIndex = 0;
    webGL.bindFramebuffer(this.frameBuffers[fboIndex]);
    webGL.clearScreen(this.clearColour);

    // Render our layers
    for (var i in this.layerDefs) {
        var layerDef = this.layerDefs[i];
        var start = layerDef.start;
        var length = layerDef.length;
        var end = start + length;
        var loop = layerDef.loop;
        if (loop) {
            if (time >= end) {
                time = start + (time - start) % length;
            }
        }
        if (time >= start && time < end) {
            var layer = layerDef.layer;
            if (layer.isFilterEffect == undefined || layer.isFilterEffect != true) {
                // Normal layer
                try {
                    layer.render(time - start, timeDelta);
                }
                catch (e) {
                    LOGMSG("Exception thrown during rendering of layer '" + layerDef.name + "', type '" + layerDef.type + "': " + e.toString());
                }
            } else {
                // Effect layer
                try {
                    webGL.bindFramebuffer(this.frameBuffers[fboIndex ^ 1]);
                    webGL.clearScreen([0, 0, 0, 0]);
                    layer.render(time - start, timeDelta, this.frameBuffers[fboIndex].diffuseTexture, this.frameBuffers[fboIndex ^ 1]);
                    fboIndex ^= 1;
                    webGL.bindFramebuffer(this.frameBuffers[fboIndex]);
                }
                catch (e) {
                    LOGMSG("Exception thrown during rendering of effect '" + layerDef.name + "', type '" + layerDef.type + "': " + e.toString());
                }
            }
        }
    }

    // Render source texture to target framebuffer if necessary
    if (this.blendMode != webGL.BLENDMODE_OPAQUE) {
        webGL.bindFramebuffer(targetFbo);
        var uniforms = {
            u_image: sourceTexture,
        };
        webGL.setBlendMode(webGL.BLENDMODE_OPAQUE);
        webGL.renderQuad(this.program, uniforms);
    }

    // Draw our result on top
    webGL.bindFramebuffer(targetFbo);
    var uniforms = {
        u_image: this.frameBuffers[fboIndex].diffuseTexture,
    };
    webGL.setBlendMode(this.blendMode);
    webGL.renderQuad(this.program, uniforms);
}

