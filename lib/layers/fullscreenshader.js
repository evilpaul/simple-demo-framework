function Layer_FullScreenShader(args) {
    this.imageFilename = args["imageFilename"];
    this.imageWrapMode = webGL.getTextureWrapModeFromString((typeof args["imageWrapMode"] !== "undefined") ? args["imageWrapMode"] : "wrap");
    this.imageSampleMode = webGL.getTextureSampleModeFromString((typeof args["imageSampleMode"] !== "undefined") ? args["imageSampleMode"] : "linear");
    this.fragmentShader = args["fragmentShader"];
    this.uniforms = (typeof args["uniforms"] !== "undefined") ? args["uniforms"] : {};
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "alpha");
}


Layer_FullScreenShader.prototype.getResourceList = function () {
    var resourceList = ["passThrough.vert", this.fragmentShader];
    if (this.imageFilename) resourceList.push(this.imageFilename);
    return resourceList;
}


Layer_FullScreenShader.prototype.initialise = function () {
    this.program = webGL.createProgram(resourceManager.getObject("passThrough.vert"), resourceManager.getObject(this.fragmentShader));
    if (this.imageFilename) {
        this.texture = webGL.createTexture(resourceManager.getObject(this.imageFilename));
        webGL.setTextureWrapMode(this.texture, this.imageWrapMode);
        webGL.setTextureSampleMode(this.texture, this.imageSampleMode);
    }
}


Layer_FullScreenShader.prototype.render = function (time, timeDelta) {
    var uniforms = {
        u_time: time,
        u_pixelSize: webGL.getPixelSize(),
        u_aspectRatio: webGL.getAspectRatio(),
        u_image: this.texture,
    };
    if (this.uniforms) {
        for (var attrName in this.uniforms) { uniforms[attrName] = this.uniforms[attrName]; }
    }

    webGL.setBlendMode(this.blendMode);
    webGL.renderQuad(this.program, uniforms);
}

