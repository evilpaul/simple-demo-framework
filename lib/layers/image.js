function Layer_Image(args) {
    this.imageFilename = args["imageFilename"];
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "alpha");
}


Layer_Image.prototype.getResourceList = function () {
    return ["passThrough.vert", "image.frag", this.imageFilename];
}


Layer_Image.prototype.initialise = function () {
    this.program = webGL.createProgram(resourceManager.getObject("passThrough.vert"), resourceManager.getObject("image.frag"));
    this.texture = webGL.createTexture(resourceManager.getObject(this.imageFilename));
}


Layer_Image.prototype.render = function (time, timeDelta) {
    var uniforms = {
        u_image: this.texture,
    };

    webGL.setBlendMode(this.blendMode);
    webGL.renderQuad(this.program, uniforms);
}

