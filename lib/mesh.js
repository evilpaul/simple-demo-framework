Mesh.programsRequired = false;
Mesh.programs = undefined;


function Mesh() {
    resourceManager.loadFile("transform3d.vert");
    resourceManager.loadFile("tinted.frag");
    resourceManager.loadFile("tintedImage.frag");
    this.isValid = false;
}


Mesh.prototype.createFromLists = function (vertices, normals, uvs, indicies) {
    this.isValid = true;

    if (vertices) {
        this.vertBuffer = webGL.createBuffer(webGL.glContext.ARRAY_BUFFER, new Float32Array(vertices));
    } else {
        LOGMSG("Failed to create mesh becuase it had no valid vertices");
        this.isValid = false;
        return;
    }

    if (normals) {
        this.normalBuffer = webGL.createBuffer(webGL.glContext.ARRAY_BUFFER, new Float32Array(normals));
    }
    else {
        this.normalBuffer = null;
    }

    if (uvs) {
        this.uvBuffer = webGL.createBuffer(webGL.glContext.ARRAY_BUFFER, new Float32Array(uvs));
    }
    else {
        this.uvBuffer = null;
    }

    if (indicies) {
        this.indexBuffer = webGL.createBuffer(webGL.glContext.ELEMENT_ARRAY_BUFFER, new Uint16Array(indicies));
        this.numIndices = indicies.length;
    } else {
        LOGMSG("Failed to create mesh becuase it had no valid indicies");
        this.isValid = false;
        return;
    }

    Mesh.programsRequired = true;
    static_meshTryLoadingPrograms();
}


Mesh.prototype.createFromObj = function (obj) {
    var sourceVertices = [];
    var sourceNormals = [];
    var sourceUVs = [];

    var vertHashes = [];
    var hashIndex = 0;

    var destVertices = [];
    var destNormals = [];
    var destUVs = [];
    var destIndicies = [];

    var lines = obj.split('\n');
    for (var i in lines) {
        var currentLine = lines[i].trim();
        var elements = currentLine.split(/\s+/);
        var command = elements.shift();

        if (command == "" || command == "#") {
            // Blank line or comment
        } else if (command == "v") {
            // Vertex
            sourceVertices.push(elements);
        } else if (command == "vn") {
            // Normal
            sourceNormals.push(elements);
        } else if (command == "vt") {
            // Texture
            sourceUVs.push([elements[0], 1 - elements[1]]);
        } else if (command == "f") {
            // Face
            var numFaceVerts = elements.length;
            if (numFaceVerts == 3 || numFaceVerts == 4) {
                // Load verts
                var faceVertHashes = [];
                for (var j = 0; j < numFaceVerts; j++) {
                    var vertexElements = elements[j].split('/');

                    // Get vert
                    var vertIndex = vertexElements[0] - 1;
                    var vertex = [sourceVertices[vertIndex][0], sourceVertices[vertIndex][1], sourceVertices[vertIndex][2]];

                    // Get normal
                    var normalIndex = vertexElements[2] == '' ? 0 : vertexElements[2] - 1;
                    if (normalIndex < sourceNormals.length) {
                        var normal = [sourceNormals[normalIndex][0], sourceNormals[normalIndex][1], sourceNormals[normalIndex][2]];
                    } else {
                        var normal = [0, 0, 1];
                    }

                    // Get uv
                    var uvIndex = vertexElements[1] == '' ? 0 : vertexElements[1] - 1;
                    if (uvIndex < sourceUVs.length) {
                        var uv = [sourceUVs[uvIndex][0], sourceUVs[uvIndex][1]];
                    } else {
                        var uv = [0, 0];
                    }

                    // 'Hash' together vert, normal and uv (it's not really a hash of course..)
                    var hash = [vertex[0] + vertex[1], vertex[2], normal[0], normal[1], normal[2], uv[0], uv[1]];
                    if (vertHashes[hash] == undefined) {
                        // We've never seen this vert before, so let's store it
                        destVertices.push(vertex[0]);
                        destVertices.push(vertex[1]);
                        destVertices.push(vertex[2]);
                        destNormals.push(normal[0]);
                        destNormals.push(normal[1]);
                        destNormals.push(normal[2]);
                        destUVs.push(uv[0]);
                        destUVs.push(uv[1]);
                        vertHashes[hash] = hashIndex++;
                    }

                    // Store hash for this vert
                    faceVertHashes.push(hash);
                }

                // Triangle
                destIndicies.push(vertHashes[faceVertHashes[0]]);
                destIndicies.push(vertHashes[faceVertHashes[1]]);
                destIndicies.push(vertHashes[faceVertHashes[2]]);
                if (numFaceVerts == 4) {
                    // Quad
                    destIndicies.push(vertHashes[faceVertHashes[2]]);
                    destIndicies.push(vertHashes[faceVertHashes[3]]);
                    destIndicies.push(vertHashes[faceVertHashes[0]]);
                }
            }
        }
    }

    // Finally.. create the mesh
    this.createFromLists(destVertices,
                         destNormals,
                         destUVs,
                         destIndicies);
}


Mesh.prototype.renderColoured = function (matWVP, colour) {
    Mesh.programsRequired = true;
    if (static_meshTryLoadingPrograms()) {
        LOGMSG("Mesh.render had to load the mesh shader programs. You probably saw a stall!");
    }

    if (this.isValid) {
        webGL.renderIndexedTriangles(Mesh.programs["coloured"], { "u_transform": matWVP, "u_colour": colour }, this.vertBuffer, { "a_texCoord": this.uvBuffer }, this.indexBuffer, this.numIndices);
    } else {
        LOGMSG("Trying to render invalid mesh");
    }
}


Mesh.prototype.renderTextured = function (matWVP, texture) {
    Mesh.programsRequired = true;
    if (static_meshTryLoadingPrograms()) {
        LOGMSG("Mesh.render had to load the mesh shader programs. You probably saw a stall!");
    }

    if (this.isValid) {
        webGL.renderIndexedTriangles(Mesh.programs["colouredTextured"], { "u_transform": matWVP, "u_image": texture, "u_colour": [1, 1, 1, 1] }, this.vertBuffer, { "a_texCoord": this.uvBuffer }, this.indexBuffer, this.numIndices);
    } else {
        LOGMSG("Trying to render invalid mesh");
    }
}


Mesh.prototype.renderColouredTextured = function (matWVP, colour, texture) {
    Mesh.programsRequired = true;
    if (static_meshTryLoadingPrograms()) {
        LOGMSG("Mesh.render had to load the mesh shader programs. You probably saw a stall!");
    }

    if (this.isValid) {
        webGL.renderIndexedTriangles(Mesh.programs["colouredTextured"], { "u_transform": matWVP, "u_image": texture, "u_colour": colour }, this.vertBuffer, { "a_texCoord": this.uvBuffer }, this.indexBuffer, this.numIndices);
    } else {
        LOGMSG("Trying to render invalid mesh");
    }
}


function static_meshTryLoadingPrograms() {
    var justLoaded = false;

    if (Mesh.programsRequired == true) {
        if (Mesh.programs == undefined) {
            if (resourceManager.isResourceLoaded("transform3d.vert") &&
                resourceManager.isResourceLoaded("tinted.frag") &&
                resourceManager.isResourceLoaded("tintedImage.frag")) {
                LOGMSG("Loading mesh shader programs..");

                Mesh.programs = []
                Mesh.programs["coloured"] = webGL.createProgram(resourceManager.getObject("transform3d.vert"), resourceManager.getObject("tinted.frag"));
                Mesh.programs["colouredTextured"] = webGL.createProgram(resourceManager.getObject("transform3d.vert"), resourceManager.getObject("tintedImage.frag"));

                justLoaded = true;
            } else {
                LOGMSG("Tried to load mesh shader programs but can't yet");
            }
        }
    }

    return justLoaded;
}

