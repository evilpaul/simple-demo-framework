var resourceManager;
var rootLayer;
var stats;
var keyQueue = [];
var webGL;


function init(config) {
    LOGMSG("Reached init()");

    // Start resource manager and register custom resource type handlers
    resourceManager = new ResourceManager(["resources/", "../lib/resources/"]);
    resourceManager.registerHandler(audioHandler, [".ogg", ".mp3"]);
    resourceManager.registerHandler(imageHandler, [".png", ".jpg", "jpeg"]);
    resourceManager.registerHandler(stringHandler, [".json"]);
    resourceManager.registerHandler(vertexShaderHandler, [".vert"]);
    resourceManager.registerHandler(fragmentShaderHandler, [".frag"]);
    resourceManager.registerHandler(meshHandler, [".obj"]);

    // Find the canvas
    var canvas = document.getElementById("mainCanvas");
    if (!canvas) {
        alert("Failed to find canvas");
        return;
    }
    webGL = new WebGL(canvas);

    // Loading screen resources
    resourceManager.loadFile("transform.vert");
    resourceManager.loadFile("image.frag");
    resourceManager.loadFile("loadingScreen.png", "loadingScreen");
    resourceManager.loadFile("touchScreenToStart.png", "touchScreenToStart");
    resourceManager.loadFile("runtimeError.png", "runtimeError");

    // Load script and audio
    this.scriptFilename = (typeof config["scriptFilename"] !== "undefined") ? config["scriptFilename"] : "script.json";
    this.audioFilename = config.audioFilename;
    this.bpm = (typeof config["bpm"] !== "undefined") ? config["bpm"] : 240;
    this.startOffset = (typeof config["startOffset"] !== "undefined") ? config["startOffset"] : 0;
    resourceManager.loadFile(this.scriptFilename, "script");
    resourceManager.loadFile(config.audioFilename, "audio");

    // Add stats?
    if (getParameterByName("stats") != null) {
        stats = new Stats();
        document.body.appendChild(stats.dom);
    }

    // Let's listen to some events
    document.addEventListener('keydown', this, true);

    // Kick out the jams..
    LOGMSG("Kicking out the jams..");
    requestAnimationFrame(mainLoop);
}
window['init'] = init;



function audioHandler(dataBuffer, url) {
    var newAudio = new Audio(url);
    return newAudio;
}


function imageHandler(dataBuffer, url) {
    var newImage = new Image();
    if (dataBuffer.size < 1024 * 100) {
        newImage.src = 'data:image;base64,' + btoa(String.fromCharCode.apply(null, dataBuffer));
    } else {
        // Large images overflow the stack, so we have to do it slightly differently (read: more slowly with less stack usage)
        newImage.src = 'data:image;base64,' + btoa([].reduce.call(new Uint8Array(dataBuffer), function (p, c) { return p + String.fromCharCode(c) }, ''));
    }
    return newImage;
}


function stringHandler(dataBuffer, url) {
    var string = String.fromCharCode.apply(null, dataBuffer);
    return string;
}


function vertexShaderHandler(dataBuffer, url) {
    var string = String.fromCharCode.apply(null, dataBuffer);
    var vertexShader = webGL.createVertexShader(string);
    if (!vertexShader) {
        LOGMSG("Vertex shader handler failed to create shader for '" + url + "'");
    }
    return vertexShader;
}


function fragmentShaderHandler(dataBuffer, url) {
    var string = String.fromCharCode.apply(null, dataBuffer);
    var fragmentShader = webGL.createFragmentShader(string);
    if (!fragmentShader) {
        LOGMSG("Fragment shader handler failed to create shader for '" + url + "'");
    }
    return fragmentShader;
}


function meshHandler(dataBuffer, url) {
    if (dataBuffer.size < 1024 * 100) {
        var string = String.fromCharCode.apply(null, dataBuffer);
    } else {
        // Large images overflow the stack, so we have to do it slightly differently (read: more slowly with less stack usage)
        var string = [].reduce.call(new Uint8Array(dataBuffer), function (p, c) { return p + String.fromCharCode(c) }, '');
    }
    var mesh = new Mesh();
    mesh.createFromObj(string);
    return mesh;
}


function tryShowScreen(textureId) {
    if (resourceManager.isResourceLoaded("transform.vert") &&
        resourceManager.isResourceLoaded("image.frag") &&
        resourceManager.isResourceLoaded(textureId)) {
        // Create the shader program
        var program = webGL.createProgram(resourceManager.getObject("transform.vert"), resourceManager.getObject("image.frag"));
        var image = resourceManager.getObject(textureId);
        var texture = webGL.createTexture(image);

        // Calculate aspect ratio and scale so that image is shown with borders if necessary
        var imageAspect = image.width / image.height;
        var screenAspect = webGL.getAspectRatio();
        if (imageAspect == screenAspect) {
            var matScale = mat3MakeScale(1, 1);
        }
        else if (imageAspect < screenAspect) {
            var matScale = mat3MakeScale(imageAspect / screenAspect, 1);
        } else {
            var matScale = mat3MakeScale(1, screenAspect / imageAspect);
        }

        // Render
        webGL.clearScreen();
        webGL.setBlendMode(webGL.BLENDMODE_OPAQUE);
        webGL.renderQuad(program, { u_image: this.texture, u_transform: matScale});

        // Clean up
        webGL.glContext.deleteTexture(texture);
        webGL.glContext.deleteProgram(program);

        return true;
    }
    else {
        return false;
    }
}

function state_loadingScreen() {
    if (tryShowScreen("loadingScreen")) {
        return "state_loadScript";
    } else {
        return "state_loadingScreen";
    }

}


function state_loadScript() {
    // Wait until the script is loaded
    if (!resourceManager.isResourceLoaded("script")) {
        // Continue in this state
        return "state_loadScript";
    } else {
        // Load script as JSON5
        var startLoadingTime = Date.now();
        LOGMSG("Loading..");
        try {
            data = {};
            data["layers"] = (JSON5.parse(resourceManager.getObject("script")));
        } catch (e) {
            LOGMSG("Error loading JSON file: " + e.toString());
            tryShowScreen("runtimeError");
            return "state_error";
        }
        LOGMSG("Loaded! (took " + ((Date.now() - startLoadingTime) / 1000) + " seconds)");

        // Load as a root group layer
        try {
            rootLayer = new Layer_Group(data);
        } catch (e) {
            LOGMSG("Error loading script: " + e.toString());
            tryShowScreen("runtimeError");
            return "state_error";
        }

        // Now go on to wait for the resources to load
        return "state_loadResources";
    }
}


function state_loadResources() {
    if (!resourceManager.isAllLoaded()) {
        // Wait for resources
        return "state_loadResources";
    } else {
        // Now all resource are loaded, we can initialise things
        var startInitialiseTime = Date.now();
        LOGMSG("Initialising..");
        try{
            rootLayer.initialise();
        } catch (e) {
            LOGMSG("Error during initialisation: " + e.toString());
            tryShowScreen("runtimeError");
            return "state_error";
        }
        LOGMSG("Initialised! (took " + ((Date.now() - startInitialiseTime) / 1000) + " seconds)");

        // This is our last chance to load the mesh programs (if they are needed) before we hit 
        // the render loop. If we wait until the render loop then we might see a stall as they are 
        // loaded on demand
        static_meshTryLoadingPrograms();

        return "state_startAudio";
    }
}


function state_startAudio() {
    var iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;
    if (iOS) {
        if (tryShowScreen("touchScreenToStart")) {
            // Add a stupid iOS hack..
            LOGMSG("Detected iOS platform. Applying audio start hack...");
            document.addEventListener('touchstart', function () {
                if (!window.audio) {
                    LOGMSG("Touch event. Starting audio..");
                    window.audio = resourceManager.getObject("audio");
                    window.audio.play();
                }
            }, false);

            return "state_iOSWait";
        } else {
            return "state_startAudio";
        }
    } else {
        // Restore transport state?
        var audioStartTime = 0;
        var audioStartPaused = false;
        if (getParameterByName("transportPersistent") != null) {
            audioStartTime = sessionStorage.getItem("position");
            audioStartPaused = sessionStorage.getItem("paused") == "true";
        }

        // Start the audio
        this.audio = resourceManager.getObject("audio");
        if (getParameterByName("mute") != null) {
            this.audio.volume = 0;
        }
        this.audio.currentTime = audioStartTime;
        if (!audioStartPaused) this.audio.play();
    }

    return "state_main";
}


function state_iOSWait() {
    if (window.audio) {
        return "state_main";
    }
    else {
        return "state_iOSWait";
    }
}


var lastUpdateTime = 0;
function state_main() {
    // First update is handled specially. It will have a delta time of 0
    if (lastUpdateTime == 0) {
        lastUpdateTime = Date.now();
    }

    // Calculate time
    var currentTime = (this.audio.currentTime - this.startOffset) * (this.bpm / 60 / 4);
    var timeNow = Date.now();
    var timeDelta = timeNow - lastUpdateTime;
    lastUpdateTime = timeNow;
    if (document.getElementById("transportText")) {
        element = document.getElementById("transportText");
        element.innerHTML = "Time: " + Math.round(currentTime * 100) / 100;
    }

    // Render
    try
    {
        webGL.bindFramebuffer(null);
        webGL.clearScreen();
        rootLayer.render(currentTime, timeDelta / 1000, null, null);
        webGL.renderAndFlushDebugTexture();
    } catch (e) {
        LOGMSG("Error during render: " + e.toString());
        webGL.bindFramebuffer(null);
        tryShowScreen("runtimeError");
    }

    // Remember transport state?
    if (getParameterByName("transportPersistent") != null) {
        sessionStorage.setItem("position", this.audio.currentTime);
        sessionStorage.setItem("paused", this.audio.paused);
    }

    // Handle key events
    while (keyQueue.length) {
        var keyCode = keyQueue.shift();
        var hasTransportControls = getParameterByName("transport") != null;
        if (keyCode == 32) {
            if (hasTransportControls) { if (audio.paused) audio.play(); else audio.pause(); }
        } else if (keyCode == 37) {
            if (hasTransportControls) { audio.currentTime = audio.currentTime - 1; }
        } else if (keyCode == 38) {
            if (hasTransportControls) { audio.currentTime = 0; }
        } else if (keyCode == 39) {
            if (hasTransportControls) { audio.currentTime = audio.currentTime + 1; }
        } else {
            // LOGMSG("Unhandled key " + keyCode);
        }
    }

    // Carry on in this state forever..
    return "state_main";
}


function state_error() {
    return "state_error";
}


var stateStartTime = Date.now();
var currentStateName = "state_loadingScreen";


function mainLoop() {
    if (stats) stats.begin();

    // Kick off the next update
    requestAnimationFrame(mainLoop);

    // Run the current state (and possibly transition to the next..)
    var nextStateName = window[currentStateName]();
    if (currentStateName != nextStateName)
    {
        LOGMSG("Changed state from '" + currentStateName + "' to '" + nextStateName + "' after " + ((Date.now() - stateStartTime) / 1000) + " seconds");
        stateStartTime = Date.now();
        currentStateName = nextStateName;
    }

    if (stats) stats.end();
}


handleEvent = function (evt) {
    if (!evt) evt = window.event;
    switch (evt.type) {
        case "keydown":
            keyQueue.push(evt.keyCode);
//             if (evt.shiftKey == true) {
//                 this.keyCodesQueue.push(evt.keyCode + 1000);
//             }
//             else {
//                 this.keyCodesQueue.push(evt.keyCode);
//             }
            break;
    }
}
