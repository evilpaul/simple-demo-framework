function Layer_Test3D(args) {
    this.meshFilename = args["meshFilename"];
    this.textureFilename = args["textureFilename"];
}


Layer_Test3D.prototype.getResourceList = function () {
    return [this.meshFilename, this.textureFilename];
}


Layer_Test3D.prototype.initialise = function () {
    this.mesh = resourceManager.getObject(this.meshFilename);
    this.texture = webGL.createTexture(resourceManager.getObject(this.textureFilename));
}


Layer_Test3D.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(webGL.BLENDMODE_OPAQUE);

    var matPersp = mat4MakePerspective(Math.PI, webGL.getAspectRatio(), 0.1, 500);
    var from = vec3Make(0, 0, -150);
    var to = vec3Make(0, 0, 0);
    var up = vec3Make(0, 1, 0);
    var matView = mat4MakeLookAt(from, to, up);
    var matViewProjection = mat4Multiply(matPersp, matView);

    var numShapes = 15;
    var matRotateX = mat4MakeRotationX(time * 2);
    var matScale = mat4MakeScale(10, 10, 10);
    var matObjectTrans = mat4MakeTranslation(50, 0, 0);
    for (var i = 0; i < numShapes; i++) {
        var alpha = i / (numShapes);
        var angle = Math.PI * 2 * alpha;
        var matRotate = mat4MakeRotationZ(angle + time);
        var matWVP = mat4MultiplyList([matViewProjection, matRotate, matObjectTrans, matScale, matRotateX]);
        this.mesh.renderColouredTextured(matWVP, [Math.sin((alpha + 0.0) * Math.PI * 2), Math.sin((alpha + 0.33) * Math.PI * 2), Math.sin((alpha + 0.66) * Math.PI * 2), 1], this.texture);
    }

    var matRotateX = mat4MakeRotationX(Math.sin(time * 0.37) * Math.PI * 2);
    var matRotateY = mat4MakeRotationY(Math.sin(time * 0.12) * Math.PI * 2);
    var matScale = mat4MakeScale(40, 40, 40);
    var matWVP = mat4MultiplyList([matViewProjection, matRotateX, matRotateY, matScale]);
    this.mesh.renderTextured(matWVP, this.texture);
}

