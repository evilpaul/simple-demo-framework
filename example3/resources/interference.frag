precision mediump float;

uniform sampler2D u_src;
uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


void main() {
    float speed = 0.1;
    float maxDisplacement = u_pixelSize.x * 20.0;
    float verticalScale = 0.05;

    float displacementR = texture2D(u_image, vec2((u_time + 0.10) * (speed + 0.48), v_texCoord.y * verticalScale)).r;
    float displacementG = texture2D(u_image, vec2((u_time + 0.21) * (speed + 0.37), v_texCoord.y * verticalScale)).g;
    float displacementB = texture2D(u_image, vec2((u_time + 0.32) * (speed + 0.26), v_texCoord.y * verticalScale)).b;
    float displacementA = texture2D(u_image, vec2((u_time + 0.43) * (speed + 0.15), v_texCoord.y * verticalScale)).a;

    float bandedDisplacementR = float(int(displacementR * 8.0)) / 8.0;
    float bandedDisplacementG = float(int(displacementG * 8.0)) / 8.0;
    float bandedDisplacementB = float(int(displacementB * 8.0)) / 8.0;
    float bandedDisplacementA = float(int(displacementA * 8.0)) / 8.0;

    float srcR = texture2D(u_src, v_texCoord + vec2((bandedDisplacementR * 2.0 - 1.0) * maxDisplacement, 0.0)).r;
    float srcG = texture2D(u_src, v_texCoord + vec2((bandedDisplacementG * 2.0 - 1.0) * maxDisplacement, 0.0)).g;
    float srcB = texture2D(u_src, v_texCoord + vec2((bandedDisplacementB * 2.0 - 1.0) * maxDisplacement, 0.0)).b;
    float srcA = texture2D(u_src, v_texCoord + vec2((bandedDisplacementA * 2.0 - 1.0) * maxDisplacement, 0.0)).a;

    gl_FragColor = vec4(srcR, srcG, srcB, srcA);
}
