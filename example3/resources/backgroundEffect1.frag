precision mediump float;

uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


void main() {
    float time = u_time * 0.71;
    float timeFraction = fract(time);
    float timeInteger = time - timeFraction;

    float sampleNow = texture2D(u_image, vec2(v_texCoord.x * 0.1, ((timeInteger + 0.0) * 0.13))).r;
    float sampleNext = texture2D(u_image, vec2(v_texCoord.x * 0.1, ((timeInteger + 1.0) * 0.13))).r;
    float value = mix(sampleNow, sampleNext, timeFraction);

    vec4 colour = mix(vec4(0.6, 0.1, 0.2, 1.0), vec4(0.0, 1.0, 0.7, 1.0), value);
    gl_FragColor = colour;
}
