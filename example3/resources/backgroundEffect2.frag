precision mediump float;

uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


void main() {
    float x = v_texCoord.x * 10.0;
    float y = u_time * 0.5 + v_texCoord.y * 7.5;
    if (fract(float(v_texCoord.x * 5.0)) < 0.5)
    {
        y = 1.0 - fract(y) + 0.125;
    }
    float value = fract(x + y);
    if (fract(value) > 0.125)
    {
        gl_FragColor = vec4(0.6, 0.1, 0.2, 1.0);
    }
    else
    {
        gl_FragColor = vec4(0.0, 1.0, 0.7, 1.0);
    }
}
