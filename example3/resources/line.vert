attribute vec2 a_position;

uniform mat3 u_transform;
uniform vec2 u_p0;
uniform vec2 u_p1;
uniform float u_halfWidth;


void main() {
    vec2 normal = normalize(u_p1 - u_p0);
    vec2 perp = vec2(-normal.y, normal.x);

    vec2 basePosition = mix(u_p0, u_p1, a_position.y);
    vec2 offset = perp * u_halfWidth * a_position.x;

    vec3 position = u_transform * vec3(basePosition + offset, 0.0);
    gl_Position = vec4(position, 1.0);
}
