precision mediump float;

uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


uniform float u_width;
uniform float u_xPosition;

#define TWOPI                   6.28318530717958647692528676656
#define NUM_POINTS              4
#define TEXTURE_REPEATS         vec2(1.0, 1.0)
#define TEXTURE_SCROLL_SPEED    -1.0


void main() {
    vec2 uv = (v_texCoord * 2.0 - 1.0) - u_xPosition;

    float ty = v_texCoord.y * TEXTURE_REPEATS.y + (sin(u_time) + u_time * 0.6) * TEXTURE_SCROLL_SPEED;
    float spin = v_texCoord.y * cos(u_time * +1.0 + 0.0) * 2.0 + 
                 v_texCoord.y * cos(u_time * -2.3 + 8.5) * 2.0 + 
                 v_texCoord.y * cos(u_time * +1.3 + 2.5) * 3.0;
    float heightOffset = sin(u_time) * 5.0;
    
    vec4 colour = vec4(0.0);
    for (int i = 0; i < NUM_POINTS; i++) {
        float p = sin(spin + (TWOPI / float(NUM_POINTS)) * float(i) + heightOffset) * u_width;
        float nextp = sin(spin + (TWOPI / float(NUM_POINTS)) * float(i + 1) + heightOffset) * u_width;
        if (uv.x > p && uv.x < nextp) {
            float tx = (uv.x - p) / (nextp - p) * TEXTURE_REPEATS.x;
            float fade = (nextp - p) / u_width;
            colour = texture2D(u_image, vec2(tx, ty)) * vec4(fade, fade, fade, 1);
            break;
        }
    }
	gl_FragColor = colour;
}
