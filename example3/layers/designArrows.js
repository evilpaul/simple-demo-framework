function Layer_DesignArrows(args) {
    this.spriteFilename = args["spriteFilename"];
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "alpha");
    this.yPos = (typeof args["yPos"] !== "undefined") ? args["yPos"] : 0;
    this.width = (typeof args["width"] !== "undefined") ? args["width"] : 0.5;
    this.numArrows = (typeof args["numArrows"] !== "undefined") ? args["numArrows"] : 100;
}


Layer_DesignArrows.prototype.getResourceList = function () {
    return ["transform.vert", "image.frag", this.spriteFilename];
}


Layer_DesignArrows.prototype.initialise = function () {
    this.program = webGL.createProgram(resourceManager.getObject("transform.vert"), resourceManager.getObject("image.frag"));
    this.texture = webGL.createTexture(resourceManager.getObject(this.spriteFilename));

    this.arrows = [];
    for (var i = 0; i < this.numArrows; i++)
    {
        var newArrow = {};
        newArrow.basePosition = vec2Make(this.yPos + (Math.random() - 0.5) * this.width, Math.random() * 3);
        var value= Math.random();
        newArrow.scale = mix(0.03, 0.1, MixFunctions.linear(value));
        newArrow.speed = mix(0.3, 1.5, MixFunctions.linear(value));
        this.arrows.push(newArrow);
    }
}


Layer_DesignArrows.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(this.blendMode);

    var matAspect = webGL.getAspectScalingMat();

    for (var i in this.arrows) {
        var arrow = this.arrows[i];
        var x = arrow.basePosition[0];
        var y = ((arrow.basePosition[1] + time * arrow.speed) % 3) - 1.5;
        var matTrans = mat3MakeTranslation(x, y);
        var matScale = mat3MakeScale(arrow.scale, arrow.scale);
        var matWVP = mat3Multiply(mat3Multiply(matTrans, matScale), matAspect);

        var uniforms = {
            u_image: this.texture,
            u_transform: matWVP,
        };

        webGL.renderQuad(this.program, uniforms);
    }
}

