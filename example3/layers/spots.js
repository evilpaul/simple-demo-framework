function Layer_Spots(args) {
    this.spriteFilename = args["spriteFilename"];
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "alpha");
    this.numColumns = (typeof args["numColumns"] !== "undefined") ? args["numColumns"] : 10;
    this.numRows = (typeof args["numRows"] !== "undefined") ? args["numRows"] : 10;
    this.minScale = (typeof args["minScale"] !== "undefined") ? args["minScale"] : 0.01;
    this.maxScale = (typeof args["maxScale"] !== "undefined") ? args["maxScale"] : 0.1;
    this.colour = (typeof args["colour"] !== "undefined") ? args["colour"] : [1, 1, 1, 1];
}


Layer_Spots.prototype.getResourceList = function () {
    return ["transform.vert", "colouredImage.frag", this.spriteFilename];
}


Layer_Spots.prototype.initialise = function () {
    this.program = webGL.createProgram(resourceManager.getObject("transform.vert"), resourceManager.getObject("colouredImage.frag"));
    this.texture = webGL.createTexture(resourceManager.getObject(this.spriteFilename));
}


Layer_Spots.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(this.blendMode);

    var matAspect = webGL.getAspectScalingMat();

    var scaledTime1 = (time + 1.0) * 1.12;
    var scaledTime2 = (time + 3.5) * 2.96;

    for (var x = 0; x < this.numColumns; x++) {
        var fx = (x / (this.numColumns - 1) * 2) - 1;
        for (var y = 0; y < this.numRows; y++) {
            var fy = (y / (this.numRows - 1) * 2) - 1;

            var value1 = Math.sin(scaledTime1 + fx * 1.12);
            var value2 = Math.sin(scaledTime1 - fy * 4.57);
            var value3 = Math.sin(scaledTime2 + fy * 2.09);
            var value4 = Math.sin(scaledTime2 - fx * 1.37);
            var value = (value1 + value2 + value3 + value4) / 8 + 0.5;
            var scale = mix(this.minScale, this.maxScale, value);

            var matTrans = mat3MakeTranslation(fx, fy);
            var matScale = mat3MakeScale(scale, scale);
            var matWVP = mat3Multiply(mat3Multiply(matTrans, matScale), matAspect);

            var uniforms = {
                u_image: this.texture,
                u_transform: matWVP,
                u_colour: this.colour,
            };

            webGL.renderQuad(this.program, uniforms);
        }
    }
}

