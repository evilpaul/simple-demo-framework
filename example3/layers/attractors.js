function Layer_Attractors(args) {
    this.spriteFilename = args["spriteFilename"];
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "alpha");
    this.numParticles = (typeof args["numParticles"] !== "undefined") ? args["numParticles"] : 100;
}


Layer_Attractors.prototype.getResourceList = function () {
    return ["transform.vert", "colouredImage.frag", this.spriteFilename];
}


Layer_Attractors.prototype.initialise = function () {
    this.program = webGL.createProgram(resourceManager.getObject("transform.vert"), resourceManager.getObject("colouredImage.frag"));
    this.texture = webGL.createTexture(resourceManager.getObject(this.spriteFilename));

    this.particles = [];
    for (var i = 0; i < this.numParticles; i++)
    {
        this.particles.push({
            "position": vec2Make(Math.random() * 2 - 1, Math.random() * 2 - 1),
            "scale": mix(0.01, 0.03, Math.random()),
            "velocity": vec2Make(0, 0),
            "alertness":  Math.random(),
        });
    }

    this.attractorLists = [
        // Central sink
        [
            {
                "origin": vec2Make(0, 0),
                "range": 2.0,
                "maxNormalForce": 0.1,
                "maxPerpForce": -0.5,
            },
        ],
        // Spread from left/right into middle
        [
            {
                "origin": vec2Make(+1.0, 0),
                "range": 0.5,
                "maxNormalForce": -1.0,
                "maxPerpForce": 0.0,
            },
            {
                "origin": vec2Make(-1.0, 0),
                "range": 0.5,
                "maxNormalForce": -1.0,
                "maxPerpForce": 0.0,
            },
        ],
        // Push down from top
        [
            {
                "origin": vec2Make(0, 1.0),
                "range": 5.0,
                "maxNormalForce": -5.0,
                "maxPerpForce": 0.0,
            },
        ],
        // Left/right sink
        [
            {
                "origin": vec2Make(+0.5, 0),
                "range": 1.5,
                "maxNormalForce": 0.0,
                "maxPerpForce": 2.5,
            },
            {
                "origin": vec2Make(-0.5, 0),
                "range": 1.5,
                "maxNormalForce": 0.0,
                "maxPerpForce": 2.5,
            },
        ],
    ];

    this.timeDeltaLeftover = 0;
}

Layer_Attractors.prototype.updateParticles = function (setIndex, timestepScale) {
    var attractors = this.attractorLists[setIndex % this.attractorLists.length];
    for (var i in attractors) {
        var attractor = attractors[i];
        var origin = attractor["origin"];
        var range = attractor["range"];
        var maxNormalForce = attractor["maxNormalForce"];
        var maxPerpForce = attractor["maxPerpForce"];
        for (var j in this.particles) {
            var particle = this.particles[j];
            var position = particle.position;
            var velocity = particle.velocity;

            var toOrigin = vec2Subtract(origin, position)
            var distance = vec2Length(toOrigin);
            if (distance <= 0.01) {
                particle.position = vec2Make(100, 100);
            } else if (distance <= range) {
                var normal = vec2Normalise(toOrigin);
                var perp = vec2Make(-normal[1], normal[0]);

                var distAlpha = Math.pow(1 - (distance / range), 2);
                var normalStrength = distAlpha * maxNormalForce * timestepScale;
                var perpStrength = distAlpha * maxPerpForce * timestepScale;

                var newForce = vec2Add(vec2Scale(normal, normalStrength), vec2Scale(perp, perpStrength));
                particle.velocity = vec2Add(velocity, newForce);
            }
        }
    }

    // Alertness checks
    for (var i in this.particles) {
        var particle = this.particles[i];
        var position = particle.position;
        var velocity = particle.velocity;
        var alertness = particle.alertness;
        var speed = vec2Length(velocity);

        if (vec2Length(position) > 2 || speed < 0.0001) {
            // If too far outside screen or moving too slowly then decreease alertness counter
            alertness -= 0.1;
            if (alertness < -1.0) {
                // If really not very alert then respawn
                position = vec2Make(Math.random() * 2 - 1, Math.random() * 2 - 1);
                velocity = vec2Make(0, 0);
                alertness = 0;
            }
        } else {
            // Still active. Increase alertness counter
            alertness = Math.min(1, particle.alertness + 0.1);
            
            // Limit max speed
            if (speed > 0.01) {
                var normal = vec2Normalise(velocity);
                velocity = vec2Scale(normal, 0.01);
            }
        }

        particle.position = vec2Add(position, velocity);
        particle.velocity = velocity;
        particle.alertness = alertness;
    }
}


Layer_Attractors.prototype.renderParticles = function () {
    webGL.setBlendMode(this.blendMode);
    var matAspect = webGL.getAspectScalingMat();

    for (var i in this.particles) {
        var particle = this.particles[i];
        var alertness = particle.alertness;

        if (alertness > 0) {
            var position = particle.position;
            var velocity = particle.velocity;
            var scale = particle.scale * alertness;
            var velocityNormal = vec2Normalise(velocity);

            var matRotation = [-velocityNormal[1], velocityNormal[0], 0,
                               velocityNormal[0], velocityNormal[1], 0,
                               0, 0, 1];
            var matTrans = mat3MakeTranslation(position[0], position[1]);
            var matScale = mat3MakeScale(scale, scale);
            var matWVP = mat3MultiplyList([matAspect, matTrans, matScale, matRotation]);

            var alpha = i / this.particles.length;
            var uniforms = {
                u_image: this.texture,
                u_transform: matWVP,
                u_colour: [alpha, 1 - alpha, 0.5, 1],
            };

            webGL.renderQuad(this.program, uniforms);
        }
    }
}


Layer_Attractors.prototype.render = function (time, timeDelta) {
    this.timeDeltaLeftover = Math.min(this.timeDeltaLeftover + timeDelta, 0.1);
    while (this.timeDeltaLeftover > 0.01) {
        this.timeDeltaLeftover -= 0.01;
        this.updateParticles(Math.trunc(time / 2), 0.005);
    }
    this.renderParticles();
}

