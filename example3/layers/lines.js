function Layer_Lines(args) {
    this.spriteFilename = args["spriteFilename"];
    this.numParticles = (typeof args["numParticles"] !== "undefined") ? args["numParticles"] : 100;
    this.maxVisibleDistance = (typeof args["maxVisibleDistance"] !== "undefined") ? args["maxVisibleDistance"] : 0.1;
    this.minSpeed = (typeof args["minSpeed"] !== "undefined") ? args["minSpeed"] : 0.1;
    this.maxSpeed = (typeof args["maxSpeed"] !== "undefined") ? args["maxSpeed"] : 0.1;
    this.lineWidth = (typeof args["lineWidth"] !== "undefined") ? args["lineWidth"] : 0.01;
}


Layer_Lines.prototype.getResourceList = function () {
    return ["line.vert", "colour.frag", this.spriteFilename];
}


Layer_Lines.prototype.initialise = function () {
    this.program = webGL.createProgram(resourceManager.getObject("line.vert"), resourceManager.getObject("colour.frag"));

    // Special vert and index buffers for drawing lines
    this.vertBuffer = webGL.createBuffer(webGL.glContext.ARRAY_BUFFER, new Float32Array([
        -1, 0, 0,
        1, 0, 0,
        -1, 1, 0,
        1, 1, 0,
    ]));
    this.indexBuffer = webGL.createBuffer(webGL.glContext.ELEMENT_ARRAY_BUFFER, new Uint16Array([
        0, 2, 1,
        2, 3, 1,
    ]));

    // Generate some particles with random velocities
    this.particles = [];
    for (var i = 0; i < this.numParticles; i++) {
        var position = vec2MakeRandomNormal();
        position[0] *= 0.5;
        position[1] *= 0.5 * webGL.getAspectRatio();
        this.particles.push({
            "position": position,
            "velocity": vec2Scale(vec2MakeRandomNormal(), mix(this.minSpeed, this.maxSpeed, Math.random())),
        });
    }

    // Logo size
    var logoHalfWidth = 1 / 2;
    var logoHalfHeight = 0.7 / 2;

    // Some fixed particles to spell out SDF
    var fixedPointLists = [
        "+-+    +  +-+",
        "-      -  -  ",
        "+-+  +-+  +-+",
        "  -  - -  -  ",
        "+-+  +-+  +  ",
    ];
    this.ParticlesToSkip = 0;
    for (var i in fixedPointLists) {
        var fixedPointList = fixedPointLists[i];
        var y = mix(logoHalfHeight, -logoHalfHeight, i / (fixedPointLists.length - 1));
        for (j in fixedPointList) {
            if (fixedPointList[j] == '+') {
                var x = mix(-logoHalfWidth, logoHalfWidth, j / (fixedPointList.length - 1));
                this.particles.push({
                    "position": vec2Make(x, y),
                    "velocity": vec2Make(0, 0),
                });
                this.ParticlesToSkip++;
            }
        }
    }

    // Line segments to spell out SDF
    var srcLineSegments = [
        // S
        [[2, 0], [0, 0]],
        [[0, 0], [0, 2]],
        [[0, 2], [2, 2]],
        [[2, 2], [2, 4]],
        [[2, 4], [0, 4]],
        // D
        [[7, 0], [7, 4]],
        [[7, 4], [5, 4]],
        [[5, 4], [5, 2]],
        [[5, 2], [7, 2]],
        // F
        [[12, 0], [10, 0]],
        [[10, 0], [10, 4]],
        [[10, 2], [12, 2]],

    ];
    this.lineSegments = [];
    for (var i in srcLineSegments) {
        var srcLineSegment  = srcLineSegments[i];
        var p0 = srcLineSegment[0];
        var p1 = srcLineSegment[1];
        var x0 = mix(-logoHalfWidth, logoHalfWidth, p0[0] / (fixedPointLists[0].length - 1));
        var y0 = mix(logoHalfHeight, -logoHalfHeight, p0[1] / (fixedPointLists.length - 1));
        var x1 = mix(-logoHalfWidth, logoHalfWidth, p1[0] / (fixedPointLists[0].length - 1));
        var y1 = mix(logoHalfHeight, -logoHalfHeight, p1[1] / (fixedPointLists.length - 1));
        this.lineSegments.push([[x0, y0], [x1, y1]]);
    }
}


Layer_Lines.prototype.drawLine = function (wvp, p0, p1, halfWidth, colour) {
    var uniforms = {
        u_transform: wvp,
        u_colour: colour,
        u_p0: p0,
        u_p1: p1,
        u_halfWidth: halfWidth,
    };
    webGL.renderIndexedTriangles(this.program, uniforms, this.vertBuffer, null, this.indexBuffer, 6);
}


Layer_Lines.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(webGL.getBlendModeFromString("additive"));
    var matAspect = webGL.getAspectScalingMat();
    var scale = mix(1.5, 1.0, MixFunctions.easeOutQuad(Math.min(time * 0.25, 1)));
    var matScale = mat3MakeScale(scale, scale);
    var matWVP = mat3Multiply(matScale, matAspect);

    // Draw the particle attractor line
    var power = 0;
    var maxVisibleDistance = this.maxVisibleDistance;
    var halfLineWidth = this.lineWidth / 2;
    for (var i = 0; i < this.particles.length - this.ParticlesToSkip; i++) {
        var particle0 = this.particles[i];
        var position0 = particle0["position"];

        // Draw links to all nearby particles
        for (var j = i + 1; j < this.particles.length; j++) {
            var particle1 = this.particles[j];
            var position1 = particle1["position"];
            var diff = vec2Subtract(position0, position1);
            var dist = vec2Length(diff);
            if (dist < maxVisibleDistance)
            {
                var distAlpha = 1 - (dist / maxVisibleDistance);
                this.drawLine(matWVP, position0, position1, halfLineWidth, [distAlpha, distAlpha * Math.abs(Math.sin(diff[0])), distAlpha * Math.abs(Math.sin(diff[1])), 1]);
                power += 0.002;
            }
        }

        // Move
        var velocity0 = particle0["velocity"];
        position0 = vec2Add(position0, vec2Scale(velocity0, timeDelta));
        if (position0[0] < -1.5) position0[0] += 3;
        else if (position0[0] > +1.5) position0[0] -= 3;
        if (position0[1] < -1.5) position0[1] += 3;
        else if (position0[1] > +1.5) position0[1] -= 3;
        particle0.position = position0;
    }

    // Overlay the logo
    power = Math.min(power, 1.0);
    for (var i in this.lineSegments) {
        var colour = power;
        var lineSegment = this.lineSegments[i];
        var p0 = [lineSegment[0][0], lineSegment[0][1]];
        var p1 = [lineSegment[1][0], lineSegment[1][1]];
        this.drawLine(matWVP, p0, p1, mix(0.0001, 0.01, power * Math.random()), [colour, colour, colour, 1]);
    }
}

