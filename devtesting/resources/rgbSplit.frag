precision mediump float;

uniform float u_time;
uniform vec2 u_pixelSize;
uniform sampler2D u_src;
uniform sampler2D u_image;

varying vec2 v_texCoord;

void main() {
    float offset = 2.0;
    vec2 offsetR = u_pixelSize * vec2(+offset, 0.0);
    vec2 offsetG = u_pixelSize * vec2(0.0, 0.0);
    vec2 offsetB = u_pixelSize * vec2(-offset, 0.0);
    vec2 ra = texture2D(u_src, v_texCoord + offsetR).ra;
    vec2 ga = texture2D(u_src, v_texCoord + offsetG).ga;
    vec2 ba = texture2D(u_src, v_texCoord + offsetB).ba;
    float a = (ra.y + ga.y + ba.y) / 3.0;
    gl_FragColor = vec4(ra.x, ga.x, ba.x, a);
}
