precision mediump float;

uniform float u_time;
uniform vec2 u_pixelSize;
uniform sampler2D u_src;
uniform sampler2D u_image;

varying vec2 v_texCoord;


#define SIZE                1.0


void main() {
    vec4 colour = (texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2( 0.0, 0.0)) + 
                   texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(+1.0,+1.0)) + 
                   texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(+1.0,-1.0)) + 
                   texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(-1.0,+1.0)) + 
                   texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(-1.0,-1.0))) / 5.0;
    gl_FragColor = colour;
}
