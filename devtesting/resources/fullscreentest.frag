precision mediump float;

//uniform sampler2D u_image;

uniform float u_time;

varying vec2 v_texCoord;

void main() {
    gl_FragColor.r = v_texCoord.x;
    gl_FragColor.g = v_texCoord.y;
    gl_FragColor.b = abs(sin(v_texCoord.y * 25.0 + u_time * 8.0));
    gl_FragColor.a = 1.0;
}

