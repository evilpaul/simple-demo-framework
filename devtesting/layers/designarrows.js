function Layer_DesignArrows(args) {
    this.spriteFilename = args["spriteFilename"];
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "alpha");
    this.numArrows = (typeof args["numArrows"] !== "undefined") ? args["numArrows"] : 100;
}


Layer_DesignArrows.prototype.getResourceList = function () {
    return ["transform.vert", "image.frag", this.spriteFilename];
}


Layer_DesignArrows.prototype.initialise = function () {
    var vertexShader = webGL.createVertexShader(resourceManager.getObject("transform.vert"));
    var fragmentShader = webGL.createFragmentShader(resourceManager.getObject("image.frag"));
    this.program = webGL.createProgram(vertexShader, fragmentShader);
    this.texture = webGL.createTexture(resourceManager.getObject(this.spriteFilename));

    this.arrows = [];
    for (var i = 0; i < this.numArrows; i++)
    {
        var newArrow = {};
        newArrow.basePosition = vec2Make(-0.75 + Math.random() * 0.5, Math.random() * 3);
        var scale = 0.05 + Math.random() * 0.05;
        newArrow.scale = scale;
        newArrow.speed = 0.1 + Math.random() * 1.5;
        this.arrows.push(newArrow);
    }
}


Layer_DesignArrows.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(this.blendMode);

    var matAspect = webGL.getAspectScalingMat();

    for (var i in this.arrows) {
        var arrow = this.arrows[i];
        var x = arrow.basePosition[0];
        var y = ((arrow.basePosition[1] + time * arrow.speed) % 3) - 1.5;
        var matTrans = mat3MakeTranslation(x, y);
        var matScale = mat3MakeScale(arrow.scale, arrow.scale);
        var matWVP = mat3Multiply(mat3Multiply(matTrans, matScale), matAspect);

        var uniforms = {
            u_image: this.texture,
            u_transform: matWVP,
        };

        webGL.renderQuad(this.program, uniforms);
    }
}

