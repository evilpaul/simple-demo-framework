function Layer_Test3D(args) {
    this.meshFilename = args["meshFilename"];
    this.textureFilename = args["textureFilename"];
}


Layer_Test3D.prototype.getResourceList = function () {
    return ["transform3d.vert", "tintedImage.frag", this.meshFilename, this.textureFilename];
}


Layer_Test3D.prototype.initialise = function () {
    var vertexShader = webGL.createVertexShader(resourceManager.getObject("transform3d.vert"));
    var fragmentShader = webGL.createFragmentShader(resourceManager.getObject("tintedImage.frag"));
    this.program = webGL.createProgram(vertexShader, fragmentShader);
    this.texture = webGL.createTexture(resourceManager.getObject(this.textureFilename));

    var mesh = resourceManager.getObject(this.meshFilename);
    this.vertBuffer = webGL.createBuffer(webGL.glContext.ARRAY_BUFFER, new Float32Array(mesh.vertices));
    this.uvBuffer = mesh.textures.length ? webGL.createBuffer(webGL.glContext.ARRAY_BUFFER, new Float32Array(mesh.textures)) : null;
    this.indexBuffer = webGL.createBuffer(webGL.glContext.ELEMENT_ARRAY_BUFFER, new Uint16Array(mesh.indices));
    this.numIndices = mesh.indices.length;
}


Layer_Test3D.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(webGL.BLENDMODE_OPAQUE);

    var matPersp = mat4MakePerspective(Math.PI, webGL.getAspectRatio(), 0.1, 500);
    var matScale = mat4MakeScale(1.75, 1.75, 1.75);
    var matWorldTrans = mat4MakeTranslation(0, 0, -150);
    var matObjectTrans = mat4MakeTranslation(50, 0, 0);

    var numShapes = 20;
    for (var i = 0; i < numShapes; i++) {
        var angle = (Math.PI * 2 / (numShapes - 1)) * i;
        var matRotate = mat4MakeRotationZ(angle + time);
        var matWVP = mat4Multiply(mat4Multiply(mat4Multiply(mat4Multiply(matPersp, matWorldTrans), matRotate), matObjectTrans), matScale);
        webGL.renderIndexedTriangles(this.program, { "u_transform": matWVP, "u_image": this.texture, "u_colour": [0.5, 0.5, 0.5, 1] }, this.vertBuffer, { "a_texCoord": this.uvBuffer }, this.indexBuffer, this.numIndices);
    }

    var angle = (Math.PI * 2 / (numShapes - 1)) * i;
    var matRotate = mat4MakeRotationY(-angle + time);
    var matScale2 = mat4MakeScale(5, 5, 5);
    var matWVP = mat4Multiply(mat4Multiply(mat4Multiply(matPersp, matWorldTrans), matRotate), matScale2);
    webGL.renderIndexedTriangles(this.program, { "u_transform": matWVP, "u_image": this.texture, "u_colour": [1, 1, 1, 1] }, this.vertBuffer, { "a_texCoord": this.uvBuffer }, this.indexBuffer, this.numIndices);
}

