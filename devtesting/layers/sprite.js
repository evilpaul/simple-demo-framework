function Layer_Sprite(args) {
    this.spriteFilename = args["spriteFilename"];
    this.blendMode = webGL.getBlendModeFromString((typeof args["blendMode"] !== "undefined") ? args["blendMode"] : "alpha");
}


Layer_Sprite.prototype.getResourceList = function () {
    return ["transform.vert", "image.frag", this.spriteFilename];
}


Layer_Sprite.prototype.initialise = function () {
    var vertexShader = webGL.createVertexShader(resourceManager.getObject("transform.vert"));
    var fragmentShader = webGL.createFragmentShader(resourceManager.getObject("image.frag"));
    this.program = webGL.createProgram(vertexShader, fragmentShader);
    this.texture = webGL.createTexture(resourceManager.getObject(this.spriteFilename));
}


Layer_Sprite.prototype.render = function (time, timeDelta) {
    webGL.setBlendMode(this.blendMode);

    var matAspect = webGL.getAspectScalingMat();
    var matScale = mat3MakeScale(0.1, 0.1);

    for (var i = 0; i < 50; i++) {
        var matTrans = mat3MakeTranslation(Math.sin(time + i * 0.15) * 0.7, Math.sin(time + i * 0.05) * 0.7);
        var matWVP = mat3Multiply(mat3Multiply(matTrans, matScale), matAspect);

        var uniforms = {
            u_time: time,
            u_image: this.texture,
            u_transform: matWVP,
        };

        webGL.renderQuad(this.program, uniforms);
    }
}

