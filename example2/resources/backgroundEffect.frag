precision mediump float;

uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


float noise1() {
    float value1 = texture2D(u_image, vec2(u_time * 0.20 - v_texCoord.y * 0.01, v_texCoord.x * 0.2)).r;
    float value2 = texture2D(u_image, vec2(u_time * 7.75 + v_texCoord.y * 1.02, u_time * 4.23 + v_texCoord.x * 1.8)).g;
    return mix(value1, value2, 0.1);
}

float noise2() {
    float value1 = texture2D(u_image, vec2(u_time * 0.50 - v_texCoord.y * 0.03, v_texCoord.x * 0.5)).r;
    float value2 = texture2D(u_image, vec2(u_time * 4.25 + v_texCoord.y * 2.17, u_time * 2.09 + v_texCoord.x * 1.5)).g;
    return mix(value1, value2, 0.1);
}

void main() {
    float value1 = noise1();
    vec4 colour1 = mix(vec4(0.2, 0.1, 0.6, 1.0), vec4(1.0, 0.7, 0.0, 1.0), value1);
    float value2 = pow(noise2(), 5.0);
    vec4 colour2 = mix(vec4(0.0, 0.0, 0.0, 1.0), vec4(2.0, 1.7, 1.2, 1.0), value2);
    gl_FragColor = colour1 + colour2;
}

