precision mediump float;

uniform float u_time;
uniform vec2 u_pixelSize;
uniform sampler2D u_src;
uniform sampler2D u_image;

varying vec2 v_texCoord;


#define WATER_LEVEL         0.3


void main() {
    vec2 sampleCoord = v_texCoord;
    float waterLevel = WATER_LEVEL + sin(u_time * 1.13) * 0.002;
    if (sampleCoord.y >  waterLevel) {
        // Above the water line
        gl_FragColor = texture2D(u_src, sampleCoord);
    } else {
        // Below the water line
        float mirrorOffset = (waterLevel - v_texCoord.y);
        float mirrorOffsetAlpha = (mirrorOffset / waterLevel);
        float perspectiveValue = pow(mirrorOffsetAlpha, 0.3);

        float xoff = sin(u_time * -2.51 + perspectiveValue * 50.0) * perspectiveValue * 0.02;
        float yoff = sin(u_time * -2.51 + perspectiveValue * 50.0) * 0.05;
        vec2 sampleCoord = v_texCoord + vec2(xoff, yoff);

        vec4 colour1 = texture2D(u_src, sampleCoord + vec2(-2.0 * u_pixelSize.x * perspectiveValue, 0.0));
        vec4 colour2 = texture2D(u_src, sampleCoord + vec2( 0.0 * u_pixelSize.x * perspectiveValue, 0.0));
        vec4 colour3 = texture2D(u_src, sampleCoord + vec2(+2.0 * u_pixelSize.x * perspectiveValue, 0.0));
        gl_FragColor = vec4(colour1.r, colour2.g, colour3.b, colour2.a) * vec4(0.8, 0.8, 0.9, 1.0);
    }
}
