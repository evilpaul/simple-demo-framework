precision mediump float;

uniform float u_time;
uniform vec2 u_pixelSize;
uniform sampler2D u_src;
uniform sampler2D u_image;

varying vec2 v_texCoord;


#define SIZE                5


void main() {
    vec4 colour = vec4(0, 0, 0, 0);
    for (int x = -SIZE; x <= SIZE; x++) {
        for (int y = -SIZE; y <= SIZE; y++) {
            colour += texture2D(u_src, v_texCoord + u_pixelSize * vec2(x, y));
        }
    }
    int kernalSize = (SIZE * 2 + 1) * (SIZE * 2 + 1);
    gl_FragColor = colour / float(kernalSize);
}
