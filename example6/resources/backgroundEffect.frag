precision mediump float;

uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


float ring(vec2 middle, float width, float base, float speed) {
    vec2 toMiddle = v_texCoord - middle;
    float distFromMiddle = length(toMiddle * vec2(u_aspectRatio, 1.0));
    float value = distFromMiddle * width - (base + u_time) * speed;
    return value;
}

float noise1() {
    float value1 = texture2D(u_image, vec2(u_time * 0.20 - v_texCoord.y * 0.01, v_texCoord.x * 0.2)).r;
    float value2 = texture2D(u_image, vec2(u_time * 7.75 + v_texCoord.y * 1.02, u_time * 4.23 + v_texCoord.x * 1.8)).g;
    return mix(value1, value2, 0.1);
}


float noise2() {
    float value1 = texture2D(u_image, vec2(u_time * 0.50 - v_texCoord.y * 0.03, v_texCoord.x * 0.5)).r;
    float value2 = texture2D(u_image, vec2(u_time * 4.25 + v_texCoord.y * 2.17, u_time * 2.09 + v_texCoord.x * 1.5)).g;
    return mix(value1, value2, 0.1);
}


void main() {
    if (v_texCoord.x < 1.0 / 3.0 * 1.0) {
        float value1 = ring(vec2(0.75, 0.7), 82.0, 0.0, 11.1);
        float value2 = ring(vec2(0.75, 0.7 + sin(u_time * 0.023) * 0.1), 11.0, 12.4, 2.6);
        float value = pow(clamp(sin(value1) + sin(value2), 0.0, 1.0), 1.1 + sin(u_time * 0.3));
        vec4 colour = mix(vec4(0.2, 0.6, 0.1, 1.0), vec4(1.0, 0.0, 0.7, 1.0), value);
        gl_FragColor = colour;
    } else if (v_texCoord.x < 1.0 / 3.0 * 2.0) {
        float value1 = noise1();
        vec4 colour1 = mix(vec4(0.2, 0.1, 0.6, 1.0), vec4(1.0, 0.7, 0.0, 1.0), value1);
        float value2 = pow(noise2(), 5.0);
        vec4 colour2 = mix(vec4(0.0, 0.0, 0.0, 1.0), vec4(2.0, 1.7, 1.2, 1.0), value2);
        gl_FragColor = colour1 + colour2;
    } else {
        float x = v_texCoord.x * 10.0;
        float y = u_time * 0.5 + v_texCoord.y * 7.5;
        if (fract(float(v_texCoord.x * 5.0)) < 0.5)
        {
            y = 1.0 - fract(y) + 0.125;
        }
        float value = fract(x + y);
        if (fract(value) > 0.125)
        {
            gl_FragColor = vec4(0.6, 0.1, 0.2, 1.0);
        }
        else
        {
            gl_FragColor = vec4(0.0, 1.0, 0.7, 1.0);
        }
    }
}
