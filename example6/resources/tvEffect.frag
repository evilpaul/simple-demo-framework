precision mediump float;

uniform float u_time;
uniform vec2 u_pixelSize;
uniform sampler2D u_src;
uniform sampler2D u_image;

varying vec2 v_texCoord;


#define PI                  3.14159265359
#define TWO_PI              6.28318530718


void main() {
    float scanlineBrightness = mix(1.0, 0.3, pow(abs(sin(v_texCoord.y * 50.0 * TWO_PI)), 6.0));

    float tear = mix(v_texCoord.y * -abs(texture2D(u_image, vec2(v_texCoord.y * 0.2 + u_time * 0.1, 0.0)).r) * 0.03, 0.0, texture2D(u_image, vec2(u_time * 0.09, 0.1)).r);
    vec2 tornCoords = v_texCoord + vec2(tear, 0);

    float srcBrightness = mix(0.7, 1.1, texture2D(u_image, vec2(u_time * 0.009, 0.2)).r);
    float ghostBrightness = mix(0.0, 0.3, texture2D(u_image, vec2(u_time * 0.013, 0.3)).r);
    float ghostOffset = mix(0.0, 0.03, texture2D(u_image, vec2(u_time * 0.004, 0.4)).r);
    vec4 src = texture2D(u_src, tornCoords);
    vec4 ghost = texture2D(u_src, tornCoords - vec2(ghostOffset, ghostOffset * -0.5));

    gl_FragColor = (src * srcBrightness + ghost * ghostBrightness) * vec4(scanlineBrightness, scanlineBrightness, scanlineBrightness, 1);
}
