precision mediump float;

uniform float u_time;
uniform vec2 u_pixelSize;
uniform sampler2D u_src;
uniform sampler2D u_image;

varying vec2 v_texCoord;


#define SIZE                10
#define STRENGTH            5.0
#define CUTOFF              0.5


void main() {
    vec3 greyscale = vec3(0.30, 0.59, 0.11);

    float total = 0.0;
    for (int x = -SIZE; x <= SIZE; x++) {
        for (int y = -SIZE; y <= SIZE; y++) {
            vec3 src = texture2D(u_src, v_texCoord + u_pixelSize * vec2(x, y)).rgb;
            float value = dot(src, greyscale);
            total += max(0.0, (value - CUTOFF) * (1.0 / CUTOFF)) * STRENGTH;
        }
    }
    int kernalSize = (SIZE * 2 + 1) * (SIZE * 2 + 1);
    float average = total / float(kernalSize);

    vec4 srcColour = texture2D(u_src, v_texCoord);
    gl_FragColor = srcColour + srcColour * average;
}
