precision mediump float;

uniform float u_time;
uniform vec2 u_pixelSize;
uniform sampler2D u_src;
uniform sampler2D u_image;

varying vec2 v_texCoord;


#define SIZE                1.0
#define STRENGTH            1.0


void main() {
    vec3 greyscale = vec3(0.30, 0.59, 0.11);

    float k11 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(-1.0,-1.0)).rgb, greyscale);
    float k12 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(-1.0, 0.0)).rgb, greyscale);
    float k13 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(-1.0,+1.0)).rgb, greyscale);
    float k21 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2( 0.0,-1.0)).rgb, greyscale);
    float k22 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2( 0.0, 0.0)).rgb, greyscale);
    float k23 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2( 0.0,+1.0)).rgb, greyscale);
    float k31 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(+1.0,-1.0)).rgb, greyscale);
    float k32 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(+1.0, 0.0)).rgb, greyscale);
    float k33 = dot(texture2D(u_src, v_texCoord + SIZE * u_pixelSize * vec2(+1.0,+1.0)).rgb, greyscale);

    float t1 = k13 + k33 + (2.0 * k23) - k11 - (2.0 * k21) - k31;
    float t2 = k31 + (2.0 * k32) + k33 - k11 - (2.0 * k12) - k13;

    float factor = sqrt(t1*t1 + t2*t2);

    vec4 srcColour = texture2D(u_src, v_texCoord);
    vec4 edgeColour = srcColour * factor;
    gl_FragColor = mix(srcColour, edgeColour, STRENGTH);
}
