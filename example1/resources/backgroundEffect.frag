precision mediump float;

uniform sampler2D u_image;
uniform float u_time;
uniform vec2 u_pixelSize;
uniform float u_aspectRatio;

varying vec2 v_texCoord;


float ring(vec2 middle, float width, float base, float speed) {
    vec2 toMiddle = v_texCoord - middle;
    float distFromMiddle = length(toMiddle * vec2(u_aspectRatio, 1.0));
    float value = distFromMiddle * width - (base + u_time) * speed;
    return value;
}

void main() {
    float value1 = ring(vec2(0.75, 0.7), 82.0, 0.0, 11.1);
    float value2 = ring(vec2(0.75, 0.7 + sin(u_time * 0.023) * 0.1), 11.0, 12.4, 2.6);
    float value = pow(clamp(sin(value1) + sin(value2), 0.0, 1.0), 1.1 + sin(u_time * 0.3));
    vec4 colour = mix(vec4(0.2, 0.6, 0.1, 1.0), vec4(1.0, 0.0, 0.7, 1.0), value);
    gl_FragColor = colour;
}
